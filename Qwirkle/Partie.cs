﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Partie
    {
        //Declaration des champs
        private readonly Joueur[] joueurs;
        private readonly int nbJoueur;

        //Declaration des méthodes
        public Partie(int p_nbJoueur)
        {
            joueurs = new Joueur[p_nbJoueur];
            this.nbJoueur = p_nbJoueur;
            Console.WriteLine("Nombre de joueurs: {0}", NbJoueur);
        }
        public void AjouterJoueur(byte index, string p_nom, int p_journais, int p_moisnais, int p_anneenais)
        {
                joueurs[index] = new Joueur(p_nom, p_journais, p_moisnais, p_anneenais);
        }
        public void AjouterJoueur(byte index, Joueur j)
        {
            AjouterJoueur(index,j.Nom, j.JourNais, j.MoisNais, j.AnneeNais);
        }

        public static bool ComparaisonDateNaissance(Joueur J1, Joueur J2)//Compare si J1 est plus vieux que J2 si oui -> retourne vrai sinon retourne faux
        {
            //Comparer 2 dates
            if (J1.AnneeNais > J2.AnneeNais)
                return false;
            else if(J1.AnneeNais<J2.AnneeNais)
                return true;
            else
            {
                if (J1.MoisNais > J2.MoisNais)
                    return false;
                else if (J1.MoisNais < J2.MoisNais)
                    return true;
                else
                {
                    if (J1.JourNais > J2.MoisNais)
                        return false;
                    else
                        return true;
                }
            }

        }
        public void InitiliasationOrdreJoueur(List<int> motJoueurs)
        {
            Joueur temp;
            bool flag = false;
            int temp1,Indmax = nbJoueur - 1, pos=nbJoueur-1;
            while (flag == false)
            {
                flag = true;
                for (int i = 0; i < nbJoueur-1; i++)
                {
                    if (motJoueurs[i] < motJoueurs[i + 1])
                    {
                        temp1 = motJoueurs[i + 1];
                        motJoueurs[i + 1] = motJoueurs[i];
                        motJoueurs[i] = temp1;

                        temp = joueurs[i + 1];
                        joueurs[i + 1] = joueurs[i];
                        joueurs[i] = temp;
                        flag = false;
                        pos = i - 1;
                    }
                    else if(motJoueurs[i+1]== motJoueurs[i])
                    {
                        if(ComparaisonDateNaissance(joueurs[i], joueurs[i + 1])==false)
                        {
                            temp1 = motJoueurs[i + 1];
                            motJoueurs[i + 1] = motJoueurs[i];
                            motJoueurs[i] = temp1;
                        }
                    }
                }
                Indmax = pos;
            }
        }

        public Joueur GetJoueurOf(int index)
        {
            return joueurs[index];
        }

        //Déclaration des accesseurs
        public int NbJoueur { get => nbJoueur; }
        public Joueur[] Joueurs { get => joueurs; }
    }
}
