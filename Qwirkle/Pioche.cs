﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Pioche
    {
        //Declaration des champs
        private List<Tuile> piocheTuile;
        private int countTuiles;
        Random aleatoire = new Random();

        //Constructeur
        public Pioche()
        {
            piocheTuile= new List<Tuile>();
            List<string> formes = new List<string> {"Losange", "Carre", "Etoile", "Croix", "Rond", "Trefle" };
            List<string> couleurs = new List<string> {"Rouge", "Orange", "Jaune", "Vert", "Bleu", "Violet" };

            //Creation des 108 tuiles
            for (int repetition =1; repetition <= 3; repetition++)
            {
                //Creation des 36 tuiles différentes
                foreach(string forme in formes)
                {
                    //Création des 6 tuiles de couleur différentes mais de la même forme
                    foreach(string couleur in couleurs)
                    {
                        Tuile tuile = new Tuile(forme, couleur);
                        //Ajout de la tuile créée à la pioche
                        piocheTuile.Add(tuile);
                    }
                }
            }

            //Initialisation du nombre de tuile dans la pioche
            countTuiles = piocheTuile.Count();
        }

        //Declaration des méthodes
        public void AjouterTuile(Tuile p_tuile)
        {
            //Ajout de la tuile à la pioche
            piocheTuile.Add(p_tuile);
            //Mise à jour du nombre de tuiles contenues dans la pioche
            countTuiles = piocheTuile.Count();
        }

        public Tuile DistributionTuile()
        {
            //Génération d'un nombre aléatoire compris entre le premier index de la pioche et le dernier
            int nbAleatoire = aleatoire.Next(0, piocheTuile.Count());
            //Récupération de la tuile situé à l'index généré aléatoirement
            Tuile temp = piocheTuile[nbAleatoire];
            //Suppression de la tuile dans la pioche
            piocheTuile.RemoveAt(nbAleatoire);
            //Mise à jour du nombre de tuiles contenues dans la pioche
            countTuiles = piocheTuile.Count();

            //Retour de la tuile récupérée
            return temp;
        }

        //Declaration des accesseurs
        public int CountTuiles { get => countTuiles; }
    }
}
