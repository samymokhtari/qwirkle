﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Joueur
    {
        //Declaration des champs
        private string nom;
        private int point;
        private int jourNais;
        private int moisNais;
        private int anneeNais;
        private List<Tuile> mainJoueur = new List<Tuile>();

        //Constructeur
        public Joueur(string p_nom, int p_jourNais, int p_moiNais, int p_anneeNais)
        {
            this.nom = p_nom;
            this.point = 0;
            this.jourNais = p_jourNais;
            this.moisNais = p_moiNais;
            this.anneeNais = p_anneeNais;
        }

        //Declaration des méthodes
        //Suppression d'une tuile dans la main du joueur(tuile posée sur le plateau ou reroll)
        public void Joue(Tuile p_tuile)
        {
            mainJoueur.RemoveAt(mainJoueur.IndexOf(p_tuile));
        }

        //Remplie la main du joueur tant que celle-ci ne contient pas 6 tuiles et que la pioche n'est pas vide
        public void RemplirMain(Pioche p_pioche)
        {
            bool piocheVide;
            if (p_pioche.CountTuiles == 0)
                piocheVide = true;
            else
                piocheVide = false;

            while(mainJoueur.Count()<6 && !piocheVide)
            {
                mainJoueur.Add(p_pioche.DistributionTuile());
                if (p_pioche.CountTuiles == 0)
                    piocheVide = true;
            }
        }

        //Compte le nombre de point obtenue par mot créé
        public void ComptagePoint(List<Tuile> tuiles)
        {
            int count = 0;

            foreach(Tuile tuile in tuiles)
            {
                count += 1;
                Console.WriteLine("+1");
            }
            if (count == 6)
                count = 12;

            point += count;
        }

        //Si le joueur pose une ou des tuiles
        //Compte les points du joueur grâce à la liste de mot et retourne un booléen si la partie est finie ou non
        public bool FinDeTour(Pioche p_pioche, List<List<Tuile>> mots) 
        {
            if(mots.Count==2)
            {
                int count=0;
                foreach (List<Tuile> mot in mots)
                    count += mot.Count;

                if(count==2)
                    point = 1;  //Si au début de la partie le joueur pose qu'une seule tuile
                else
                    foreach (List<Tuile> mot in mots)
                        //Si un mot ne comprend qu'une seul tuiles, il n'est pas comptabilisé
                        if (mot.Count() > 1) 
                            ComptagePoint(mot);
            }
            else
            {
                foreach (List<Tuile> mot in mots)
                    //Si un mot ne comprend qu'une seul tuile, il n'est pas comptabilisé
                    if (mot.Count() > 1)
                        ComptagePoint(mot);
            }
            RemplirMain(p_pioche);

            //Si la partie est finie
            if (p_pioche.CountTuiles == 0 && mainJoueur.Count() == 0)
            {
                point += 6; // points bonus pour avoir vidé sa main
                return true;
            }

            return false;
        }

        //Si le joueur reroll ses tuiles
        //Reroll toutes les tuiles de la liste puis remplie la main
        public void FinDeTour(Pioche p_pioche, List<Tuile> tuilesReroll) 
        {
            foreach(Tuile tuile in tuilesReroll)
            {
                Reroll(p_pioche, tuile);
            }
            RemplirMain(p_pioche);
        }

        //Permet de récupérer l'index d'une tuile de la main du joueur
        public Tuile RecupererTuile(byte index)
        {
            return mainJoueur.ElementAt(index);
        }
        
        //Renvoie la tuile dans la pioche et la supprime de la main du joueur
        public void Reroll(Pioche p_pioche, Tuile p_tuile)
        {
            p_pioche.AjouterTuile(p_tuile);
            Joue(p_tuile);
        }

        //Surcharges d'opérateurs
        public static bool operator ==(Joueur j1, Joueur j2)
        {
            return j1.nom == j2.nom && j1.point==j2.point && j1.jourNais == j2.jourNais && j1.MoisNais == j2.MoisNais && j1.AnneeNais == j2.AnneeNais;
        }

        public static bool operator !=(Joueur j1, Joueur j2)
        {
            return !(j1 == j2);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.nom.GetHashCode();
            hash ^= this.point.GetHashCode();
            hash ^= this.jourNais.GetHashCode();
            hash ^= this.moisNais.GetHashCode();
            hash ^= this.AnneeNais.GetHashCode();

            return hash;
        }

        public override bool Equals(object obj)
        {
            return obj is Joueur && this == (Joueur)obj;

        }

        //Déclaration des accesseurs
        public string Nom { get => nom; set => nom = value; }
        public int Point { get => point; set => point = value; }
        public int JourNais { get => jourNais; set => jourNais = value; }
        public int MoisNais { get => moisNais; set => moisNais = value; }
        public int AnneeNais { get => anneeNais; set => anneeNais = value; }
        public List<Tuile> MainJoueur { get => mainJoueur; }
    }
}
