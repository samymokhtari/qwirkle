﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Qwirkle
{
    public class Tuile
    {
        //Declarations des champs
        private readonly string forme;
        private readonly string couleur;
        private readonly string image;

        //Constructeur
        public Tuile(string p_forme, string p_couleur)
        {
            this.forme = p_forme;
            this.couleur = p_couleur;
            //Creation du chemin d'accès de l'image a partir de la couleur et de la forme de la tuile
            this.image = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\..\Ressources\images\", forme+couleur+".jpg"));
        }

        //Declaration des méthodes
        //Vérification de la compatibilité avec les autres tuiles comprises dans la liste
        public bool Compatible(List<Tuile> tuiles)
        {
            bool forme_compatible = false;
            bool couleur_compatible = false;
            
            //Vérification tuile par tuile
            foreach(Tuile tuile in tuiles)
            {
                //Compatible par la couleur et les tuiles vérifiées précédemment ne sont pas compatible par la forme
                if(couleur==tuile.Couleur && forme!=tuile.Forme && !forme_compatible)
                {
                    couleur_compatible = true;
                }
                else
                {
                    //Compatible par la forme et les tuiles vérifiées précédemment ne sont pas compatible par la couleur
                    if (forme==tuile.Forme && couleur!=tuile.Couleur && !couleur_compatible)
                    {
                        forme_compatible = true;
                    }
                    //Compatible par la couleur mais les tuiles vérifiées précédemment sont compatible par la forme ou l'inverse
                    //ou les tuiles ne sont tout simplement pas compatible
                    else
                    {
                        return false;
                    }
                }
            }
            //Si on sort de la boucle alors la tuile est compatible avec la liste
            return true;
        }


        //Surcharges d'opérateurs
        public static bool operator ==(Tuile t1, Tuile t2)
        {
            return t1.forme == t2.forme && t1.couleur == t2.couleur && t1.image == t2.image;
        }

        public static bool operator !=(Tuile t1, Tuile t2)
        {
            return !(t1 == t2);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.forme.GetHashCode();
            hash ^= this.couleur.GetHashCode();
            hash ^= this.image.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            return obj is Tuile && this == (Tuile)obj;

        }

        //Declaration des accesseurs
        public string Forme { get => forme; }
        public string Couleur { get => couleur; }
        public string Image { get => image; }
    }

}
