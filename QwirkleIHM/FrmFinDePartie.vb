﻿Imports Qwirkle
Public Class FrmFinDePartie
    Private Sub FrmFinDePartie_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PnlPointJ3.Hide()
        PnlPointJ4.Hide()
        Dim J1 = FrmJeu.partie.GetJoueurOf(0)
        LblNomJoueur1.Text = J1.Nom
        LblPoint1.Text = J1.Point
        Dim J2 = FrmJeu.partie.GetJoueurOf(1)
        LblNomJoueur2.Text = J2.Nom
        LblPoint2.Text = J2.Point
        Dim J3 = FrmJeu.partie.GetJoueurOf(2)
        If (FrmJeu.partie.NbJoueur > 2) Then
            LblNomJoueur3.Text = J3.Nom
            LblPoint3.Text = J3.Point
            PnlPointJ3.Show()
        End If
        Dim J4 = FrmJeu.partie.GetJoueurOf(3)
        If (FrmJeu.partie.NbJoueur > 3) Then
            LblPoint4.Text = J4.Point
            LblNomJoueur4.Text = J4.Nom
            PnlPointJ4.Show()
        End If


        If (FrmJeu.partie.NbJoueur = 2) Then
            If (J1.Point > J2.Point) Then
                LblNomJoueurVictoire.Text = J1.Nom
            Else
                LblNomJoueurVictoire.Text = J2.Nom
            End If
        ElseIf (FrmJeu.partie.NbJoueur = 3) Then
            If (J1.Point > J2.Point And J1.Point > J3.Point) Then
                LblNomJoueurVictoire.Text = J1.Nom
            End If
            If (J2.Point > J1.Point And J2.Point > J3.Point) Then
                LblNomJoueurVictoire.Text = J2.Nom
            End If
            If (J3.Point > J1.Point And J3.Point > J2.Point) Then
                LblNomJoueurVictoire.Text = J3.Nom
            End If
        ElseIf (FrmJeu.partie.NbJoueur = 4) Then
            If (J1.Point > J2.Point And J1.Point > J3.Point And J1.Point > J4.Point) Then
                LblNomJoueurVictoire.Text = J1.Nom
            End If
            If (J2.Point > J1.Point And J2.Point > J3.Point And J1.Point > J4.Point) Then
                LblNomJoueurVictoire.Text = J2.Nom
            End If
            If (J3.Point > J1.Point And J3.Point > J2.Point And J3.Point > J4.Point) Then
                LblNomJoueurVictoire.Text = J3.Nom
            End If
            If (J4.Point > J1.Point And J4.Point > J2.Point And J4.Point > J3.Point) Then
                LblNomJoueurVictoire.Text = J4.Nom
            End If
        End If

    End Sub

    Private Sub BtnQuitter_Click(sender As Object, e As EventArgs) Handles BtnQuitter.Click
        Me.Close()
    End Sub
End Class