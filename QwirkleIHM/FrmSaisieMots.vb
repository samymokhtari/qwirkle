﻿Imports QwirkleIHM
Imports Qwirkle
Public Class FrmSaisieMots
    Dim listeMot As List(Of Integer) = New List(Of Integer)
    Dim i As Byte = 0

    Public Property ListeMot1 As List(Of Integer)
        Get
            Return listeMot
        End Get
        Set(value As List(Of Integer))
            listeMot = value
        End Set
    End Property

    Public Sub SaisieMotJoueur(j As Joueur)
        Dim myValue As Integer
        myValue = CInt(tbxSaisieMot.Text)
        listeMot.Add(myValue)
    End Sub


    Private Sub FrmSaisieMots_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Byte = 0
        Dim ValeurMot As New NumericUpDown()
        FrmJeu.AfficherMainJoueur(FrmJeu.partie.GetJoueurOf(0), pnlMainJoueur)
        lblNomJoueur.Text = FrmJeu.partie.GetJoueurOf(0).Nom
    End Sub

    Private Sub TbxSaisieMot_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbxSaisieMot.KeyPress
        If (Char.IsNumber(e.KeyChar) = False) And (e.KeyChar <> ControlChars.Back) Then
            e.Handled = True
        End If
    End Sub

    Private Sub CmdValiderSaisie_Click(sender As Object, e As EventArgs) Handles cmdValiderSaisie.Click
        If (i < FrmJeu.partie.NbJoueur) Then
            SaisieMotJoueur(FrmJeu.partie.GetJoueurOf(i))
            If (i <> FrmJeu.partie.NbJoueur - 1) Then
                lblNomJoueur.Text = FrmJeu.partie.GetJoueurOf(i + 1).Nom
                pnlMainJoueur.Hide()
                MessageBox.Show("Continuer", "Joueur suivant", MessageBoxButtons.OK)
                FrmJeu.AfficherMainJoueur(FrmJeu.partie.GetJoueurOf(i + 1), pnlMainJoueur)
                pnlMainJoueur.Show()
            Else
                Me.Close()
            End If
            i = i + 1
        End If

    End Sub

End Class