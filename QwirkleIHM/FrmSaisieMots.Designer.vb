﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSaisieMots
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSaisieMots))
        Me.pnlMainJoueur = New System.Windows.Forms.Panel()
        Me.pbx6 = New System.Windows.Forms.PictureBox()
        Me.pbx5 = New System.Windows.Forms.PictureBox()
        Me.pbx4 = New System.Windows.Forms.PictureBox()
        Me.pbx3 = New System.Windows.Forms.PictureBox()
        Me.pbx2 = New System.Windows.Forms.PictureBox()
        Me.pbx1 = New System.Windows.Forms.PictureBox()
        Me.tbxSaisieMot = New System.Windows.Forms.TextBox()
        Me.cmdValiderSaisie = New System.Windows.Forms.Button()
        Me.lblTitre = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblMainde = New System.Windows.Forms.Label()
        Me.lblNomJoueur = New System.Windows.Forms.Label()
        Me.pnlMainJoueur.SuspendLayout()
        CType(Me.pbx6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainJoueur
        '
        Me.pnlMainJoueur.Controls.Add(Me.pbx6)
        Me.pnlMainJoueur.Controls.Add(Me.pbx5)
        Me.pnlMainJoueur.Controls.Add(Me.pbx4)
        Me.pnlMainJoueur.Controls.Add(Me.pbx3)
        Me.pnlMainJoueur.Controls.Add(Me.pbx2)
        Me.pnlMainJoueur.Controls.Add(Me.pbx1)
        Me.pnlMainJoueur.Location = New System.Drawing.Point(16, 278)
        Me.pnlMainJoueur.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlMainJoueur.Name = "pnlMainJoueur"
        Me.pnlMainJoueur.Size = New System.Drawing.Size(620, 129)
        Me.pnlMainJoueur.TabIndex = 0
        '
        'pbx6
        '
        Me.pbx6.Location = New System.Drawing.Point(536, 27)
        Me.pbx6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx6.Name = "pbx6"
        Me.pbx6.Size = New System.Drawing.Size(80, 74)
        Me.pbx6.TabIndex = 5
        Me.pbx6.TabStop = False
        '
        'pbx5
        '
        Me.pbx5.Location = New System.Drawing.Point(436, 27)
        Me.pbx5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx5.Name = "pbx5"
        Me.pbx5.Size = New System.Drawing.Size(80, 74)
        Me.pbx5.TabIndex = 4
        Me.pbx5.TabStop = False
        '
        'pbx4
        '
        Me.pbx4.Location = New System.Drawing.Point(325, 27)
        Me.pbx4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx4.Name = "pbx4"
        Me.pbx4.Size = New System.Drawing.Size(80, 74)
        Me.pbx4.TabIndex = 3
        Me.pbx4.TabStop = False
        '
        'pbx3
        '
        Me.pbx3.Location = New System.Drawing.Point(212, 27)
        Me.pbx3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx3.Name = "pbx3"
        Me.pbx3.Size = New System.Drawing.Size(80, 74)
        Me.pbx3.TabIndex = 2
        Me.pbx3.TabStop = False
        '
        'pbx2
        '
        Me.pbx2.Location = New System.Drawing.Point(111, 27)
        Me.pbx2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx2.Name = "pbx2"
        Me.pbx2.Size = New System.Drawing.Size(80, 74)
        Me.pbx2.TabIndex = 1
        Me.pbx2.TabStop = False
        '
        'pbx1
        '
        Me.pbx1.Location = New System.Drawing.Point(4, 27)
        Me.pbx1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbx1.Name = "pbx1"
        Me.pbx1.Size = New System.Drawing.Size(80, 74)
        Me.pbx1.TabIndex = 0
        Me.pbx1.TabStop = False
        '
        'tbxSaisieMot
        '
        Me.tbxSaisieMot.Location = New System.Drawing.Point(247, 463)
        Me.tbxSaisieMot.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbxSaisieMot.Name = "tbxSaisieMot"
        Me.tbxSaisieMot.Size = New System.Drawing.Size(132, 22)
        Me.tbxSaisieMot.TabIndex = 1
        Me.tbxSaisieMot.Text = "0"
        '
        'cmdValiderSaisie
        '
        Me.cmdValiderSaisie.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdValiderSaisie.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdValiderSaisie.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdValiderSaisie.Location = New System.Drawing.Point(405, 458)
        Me.cmdValiderSaisie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdValiderSaisie.Name = "cmdValiderSaisie"
        Me.cmdValiderSaisie.Size = New System.Drawing.Size(100, 28)
        Me.cmdValiderSaisie.TabIndex = 2
        Me.cmdValiderSaisie.Text = "Valider"
        Me.cmdValiderSaisie.UseVisualStyleBackColor = False
        '
        'lblTitre
        '
        Me.lblTitre.AutoSize = True
        Me.lblTitre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitre.Location = New System.Drawing.Point(120, 23)
        Me.lblTitre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTitre.Name = "lblTitre"
        Me.lblTitre.Size = New System.Drawing.Size(379, 31)
        Me.lblTitre.TabIndex = 3
        Me.lblTitre.Text = "Saisie des mots des joueurs"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(55, 78)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(556, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Les joueurs doivent saisir leur potentiel mot le plus long pour définir l'ordre d" &
    "e passage"
        '
        'lblMainde
        '
        Me.lblMainde.AutoSize = True
        Me.lblMainde.Location = New System.Drawing.Point(228, 239)
        Me.lblMainde.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMainde.Name = "lblMainde"
        Me.lblMainde.Size = New System.Drawing.Size(66, 17)
        Me.lblMainde.TabIndex = 5
        Me.lblMainde.Text = "Main de :"
        '
        'lblNomJoueur
        '
        Me.lblNomJoueur.AutoSize = True
        Me.lblNomJoueur.Location = New System.Drawing.Point(304, 239)
        Me.lblNomJoueur.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNomJoueur.Name = "lblNomJoueur"
        Me.lblNomJoueur.Size = New System.Drawing.Size(42, 17)
        Me.lblNomJoueur.TabIndex = 6
        Me.lblNomJoueur.Text = "<val>"
        '
        'FrmSaisieMots
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(652, 603)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblNomJoueur)
        Me.Controls.Add(Me.lblMainde)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTitre)
        Me.Controls.Add(Me.cmdValiderSaisie)
        Me.Controls.Add(Me.tbxSaisieMot)
        Me.Controls.Add(Me.pnlMainJoueur)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximumSize = New System.Drawing.Size(670, 650)
        Me.MinimumSize = New System.Drawing.Size(670, 650)
        Me.Name = "FrmSaisieMots"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Préparation de la partie"
        Me.pnlMainJoueur.ResumeLayout(False)
        CType(Me.pbx6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnlMainJoueur As Panel
    Friend WithEvents pbx6 As PictureBox
    Friend WithEvents pbx5 As PictureBox
    Friend WithEvents pbx4 As PictureBox
    Friend WithEvents pbx3 As PictureBox
    Friend WithEvents pbx2 As PictureBox
    Friend WithEvents pbx1 As PictureBox
    Friend WithEvents tbxSaisieMot As TextBox
    Friend WithEvents cmdValiderSaisie As Button
    Friend WithEvents lblTitre As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblMainde As Label
    Friend WithEvents lblNomJoueur As Label
End Class
