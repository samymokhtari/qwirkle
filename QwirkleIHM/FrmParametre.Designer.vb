﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmParametre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmParametre))
        Me.pnlNbJoueurs = New System.Windows.Forms.Panel()
        Me.RbtDeux = New System.Windows.Forms.RadioButton()
        Me.RbtTrois = New System.Windows.Forms.RadioButton()
        Me.RbtQuatre = New System.Windows.Forms.RadioButton()
        Me.LblNombreJoueur = New System.Windows.Forms.Label()
        Me.TxtNomJ2 = New System.Windows.Forms.TextBox()
        Me.DtpJ2 = New System.Windows.Forms.DateTimePicker()
        Me.TxtNomJ3 = New System.Windows.Forms.TextBox()
        Me.DtpJ3 = New System.Windows.Forms.DateTimePicker()
        Me.TxtNomJ4 = New System.Windows.Forms.TextBox()
        Me.DtpJ4 = New System.Windows.Forms.DateTimePicker()
        Me.tltParametres = New System.Windows.Forms.ToolTip(Me.components)
        Me.GpbJoueur = New System.Windows.Forms.GroupBox()
        Me.PnlJ4 = New System.Windows.Forms.Panel()
        Me.PnlJ3 = New System.Windows.Forms.Panel()
        Me.PnlJ2 = New System.Windows.Forms.Panel()
        Me.PnlJ1 = New System.Windows.Forms.Panel()
        Me.TxtNomJ1 = New System.Windows.Forms.TextBox()
        Me.DtpJ1 = New System.Windows.Forms.DateTimePicker()
        Me.CmdJouer = New System.Windows.Forms.Button()
        Me.LblRegle = New System.Windows.Forms.LinkLabel()
        Me.PtbTitre = New System.Windows.Forms.PictureBox()
        Me.pnlNbJoueurs.SuspendLayout()
        Me.GpbJoueur.SuspendLayout()
        Me.PnlJ4.SuspendLayout()
        Me.PnlJ3.SuspendLayout()
        Me.PnlJ2.SuspendLayout()
        Me.PnlJ1.SuspendLayout()
        CType(Me.PtbTitre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlNbJoueurs
        '
        Me.pnlNbJoueurs.Controls.Add(Me.RbtDeux)
        Me.pnlNbJoueurs.Controls.Add(Me.RbtTrois)
        Me.pnlNbJoueurs.Controls.Add(Me.RbtQuatre)
        Me.pnlNbJoueurs.Location = New System.Drawing.Point(240, 140)
        Me.pnlNbJoueurs.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlNbJoueurs.Name = "pnlNbJoueurs"
        Me.pnlNbJoueurs.Size = New System.Drawing.Size(469, 62)
        Me.pnlNbJoueurs.TabIndex = 15
        '
        'RbtDeux
        '
        Me.RbtDeux.Appearance = System.Windows.Forms.Appearance.Button
        Me.RbtDeux.AutoSize = True
        Me.RbtDeux.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.5!)
        Me.RbtDeux.Location = New System.Drawing.Point(12, 6)
        Me.RbtDeux.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RbtDeux.Name = "RbtDeux"
        Me.RbtDeux.Size = New System.Drawing.Size(108, 49)
        Me.RbtDeux.TabIndex = 3
        Me.RbtDeux.TabStop = True
        Me.RbtDeux.Tag = "2"
        Me.RbtDeux.Text = "Deux"
        Me.RbtDeux.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RbtDeux.UseVisualStyleBackColor = True
        '
        'RbtTrois
        '
        Me.RbtTrois.Appearance = System.Windows.Forms.Appearance.Button
        Me.RbtTrois.AutoSize = True
        Me.RbtTrois.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.5!)
        Me.RbtTrois.Location = New System.Drawing.Point(169, 6)
        Me.RbtTrois.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RbtTrois.Name = "RbtTrois"
        Me.RbtTrois.Size = New System.Drawing.Size(105, 49)
        Me.RbtTrois.TabIndex = 4
        Me.RbtTrois.TabStop = True
        Me.RbtTrois.Tag = "3"
        Me.RbtTrois.Text = "Trois"
        Me.RbtTrois.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RbtTrois.UseVisualStyleBackColor = True
        '
        'RbtQuatre
        '
        Me.RbtQuatre.Appearance = System.Windows.Forms.Appearance.Button
        Me.RbtQuatre.AutoSize = True
        Me.RbtQuatre.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.5!)
        Me.RbtQuatre.Location = New System.Drawing.Point(316, 5)
        Me.RbtQuatre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RbtQuatre.Name = "RbtQuatre"
        Me.RbtQuatre.Size = New System.Drawing.Size(133, 49)
        Me.RbtQuatre.TabIndex = 5
        Me.RbtQuatre.TabStop = True
        Me.RbtQuatre.Tag = "4"
        Me.RbtQuatre.Text = "Quatre"
        Me.RbtQuatre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RbtQuatre.UseVisualStyleBackColor = True
        '
        'LblNombreJoueur
        '
        Me.LblNombreJoueur.AutoSize = True
        Me.LblNombreJoueur.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblNombreJoueur.Location = New System.Drawing.Point(50, 160)
        Me.LblNombreJoueur.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblNombreJoueur.Name = "LblNombreJoueur"
        Me.LblNombreJoueur.Size = New System.Drawing.Size(168, 22)
        Me.LblNombreJoueur.TabIndex = 1
        Me.LblNombreJoueur.Text = "Nombre de joueur : "
        '
        'TxtNomJ2
        '
        Me.TxtNomJ2.Location = New System.Drawing.Point(48, 4)
        Me.TxtNomJ2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtNomJ2.Name = "TxtNomJ2"
        Me.TxtNomJ2.Size = New System.Drawing.Size(132, 22)
        Me.TxtNomJ2.TabIndex = 0
        Me.TxtNomJ2.Tag = "2"
        Me.TxtNomJ2.Text = "Nom Joueur 2"
        '
        'DtpJ2
        '
        Me.DtpJ2.Location = New System.Drawing.Point(255, 2)
        Me.DtpJ2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DtpJ2.MinDate = New Date(1800, 1, 1, 0, 0, 0, 0)
        Me.DtpJ2.Name = "DtpJ2"
        Me.DtpJ2.Size = New System.Drawing.Size(265, 22)
        Me.DtpJ2.TabIndex = 4
        Me.DtpJ2.Tag = "2"
        Me.DtpJ2.Value = New Date(1800, 1, 1, 0, 0, 0, 0)
        '
        'TxtNomJ3
        '
        Me.TxtNomJ3.Location = New System.Drawing.Point(48, 7)
        Me.TxtNomJ3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtNomJ3.Name = "TxtNomJ3"
        Me.TxtNomJ3.Size = New System.Drawing.Size(132, 22)
        Me.TxtNomJ3.TabIndex = 0
        Me.TxtNomJ3.Tag = "3"
        Me.TxtNomJ3.Text = "Nom Joueur 3"
        '
        'DtpJ3
        '
        Me.DtpJ3.Location = New System.Drawing.Point(255, 4)
        Me.DtpJ3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DtpJ3.MinDate = New Date(1800, 1, 1, 0, 0, 0, 0)
        Me.DtpJ3.Name = "DtpJ3"
        Me.DtpJ3.Size = New System.Drawing.Size(265, 22)
        Me.DtpJ3.TabIndex = 4
        Me.DtpJ3.Tag = "3"
        Me.DtpJ3.Value = New Date(1800, 1, 1, 0, 0, 0, 0)
        '
        'TxtNomJ4
        '
        Me.TxtNomJ4.Location = New System.Drawing.Point(48, 7)
        Me.TxtNomJ4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtNomJ4.Name = "TxtNomJ4"
        Me.TxtNomJ4.Size = New System.Drawing.Size(132, 22)
        Me.TxtNomJ4.TabIndex = 0
        Me.TxtNomJ4.Tag = "4"
        Me.TxtNomJ4.Text = "Nom Joueur 4"
        '
        'DtpJ4
        '
        Me.DtpJ4.Location = New System.Drawing.Point(255, 4)
        Me.DtpJ4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DtpJ4.MinDate = New Date(1800, 1, 1, 0, 0, 0, 0)
        Me.DtpJ4.Name = "DtpJ4"
        Me.DtpJ4.Size = New System.Drawing.Size(265, 22)
        Me.DtpJ4.TabIndex = 4
        Me.DtpJ4.Tag = "4"
        Me.DtpJ4.Value = New Date(1800, 1, 1, 0, 0, 0, 0)
        '
        'GpbJoueur
        '
        Me.GpbJoueur.Controls.Add(Me.PnlJ4)
        Me.GpbJoueur.Controls.Add(Me.PnlJ3)
        Me.GpbJoueur.Controls.Add(Me.PnlJ2)
        Me.GpbJoueur.Controls.Add(Me.PnlJ1)
        Me.GpbJoueur.Location = New System.Drawing.Point(50, 230)
        Me.GpbJoueur.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GpbJoueur.Name = "GpbJoueur"
        Me.GpbJoueur.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GpbJoueur.Size = New System.Drawing.Size(675, 195)
        Me.GpbJoueur.TabIndex = 14
        Me.GpbJoueur.TabStop = False
        Me.GpbJoueur.Text = "Pseudo des joueurs et date de naissance :"
        '
        'PnlJ4
        '
        Me.PnlJ4.Controls.Add(Me.TxtNomJ4)
        Me.PnlJ4.Controls.Add(Me.DtpJ4)
        Me.PnlJ4.Location = New System.Drawing.Point(12, 150)
        Me.PnlJ4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PnlJ4.Name = "PnlJ4"
        Me.PnlJ4.Size = New System.Drawing.Size(637, 33)
        Me.PnlJ4.TabIndex = 6
        '
        'PnlJ3
        '
        Me.PnlJ3.Controls.Add(Me.TxtNomJ3)
        Me.PnlJ3.Controls.Add(Me.DtpJ3)
        Me.PnlJ3.Location = New System.Drawing.Point(12, 109)
        Me.PnlJ3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PnlJ3.Name = "PnlJ3"
        Me.PnlJ3.Size = New System.Drawing.Size(637, 33)
        Me.PnlJ3.TabIndex = 5
        '
        'PnlJ2
        '
        Me.PnlJ2.Controls.Add(Me.TxtNomJ2)
        Me.PnlJ2.Controls.Add(Me.DtpJ2)
        Me.PnlJ2.Location = New System.Drawing.Point(12, 69)
        Me.PnlJ2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PnlJ2.Name = "PnlJ2"
        Me.PnlJ2.Size = New System.Drawing.Size(637, 32)
        Me.PnlJ2.TabIndex = 5
        '
        'PnlJ1
        '
        Me.PnlJ1.Controls.Add(Me.TxtNomJ1)
        Me.PnlJ1.Controls.Add(Me.DtpJ1)
        Me.PnlJ1.Location = New System.Drawing.Point(12, 23)
        Me.PnlJ1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PnlJ1.Name = "PnlJ1"
        Me.PnlJ1.Size = New System.Drawing.Size(637, 38)
        Me.PnlJ1.TabIndex = 0
        '
        'TxtNomJ1
        '
        Me.TxtNomJ1.Location = New System.Drawing.Point(48, 7)
        Me.TxtNomJ1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtNomJ1.Name = "TxtNomJ1"
        Me.TxtNomJ1.Size = New System.Drawing.Size(132, 22)
        Me.TxtNomJ1.TabIndex = 0
        Me.TxtNomJ1.Tag = "1"
        Me.TxtNomJ1.Text = "Nom Joueur 1"
        '
        'DtpJ1
        '
        Me.DtpJ1.Location = New System.Drawing.Point(255, 6)
        Me.DtpJ1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DtpJ1.MinDate = New Date(1800, 1, 1, 0, 0, 0, 0)
        Me.DtpJ1.Name = "DtpJ1"
        Me.DtpJ1.Size = New System.Drawing.Size(265, 22)
        Me.DtpJ1.TabIndex = 4
        Me.DtpJ1.Tag = "1"
        Me.DtpJ1.Value = New Date(1800, 1, 1, 0, 0, 0, 0)
        '
        'CmdJouer
        '
        Me.CmdJouer.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.CmdJouer.AutoSize = True
        Me.CmdJouer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CmdJouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmdJouer.Location = New System.Drawing.Point(300, 500)
        Me.CmdJouer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CmdJouer.Name = "CmdJouer"
        Me.CmdJouer.Size = New System.Drawing.Size(200, 80)
        Me.CmdJouer.TabIndex = 13
        Me.CmdJouer.Text = "Jouer"
        Me.CmdJouer.UseVisualStyleBackColor = True
        '
        'LblRegle
        '
        Me.LblRegle.AutoSize = True
        Me.LblRegle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblRegle.Location = New System.Drawing.Point(80, 60)
        Me.LblRegle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblRegle.Name = "LblRegle"
        Me.LblRegle.Size = New System.Drawing.Size(57, 22)
        Me.LblRegle.TabIndex = 12
        Me.LblRegle.TabStop = True
        Me.LblRegle.Text = "Règle"
        '
        'PtbTitre
        '
        Me.PtbTitre.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PtbTitre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PtbTitre.Image = Global.QwirkleIHM.My.Resources.Resources.qwirkle_logo
        Me.PtbTitre.InitialImage = Global.QwirkleIHM.My.Resources.Resources.qwirkle_logo
        Me.PtbTitre.Location = New System.Drawing.Point(200, 20)
        Me.PtbTitre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PtbTitre.Name = "PtbTitre"
        Me.PtbTitre.Size = New System.Drawing.Size(400, 100)
        Me.PtbTitre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTitre.TabIndex = 11
        Me.PtbTitre.TabStop = False
        '
        'FrmParametre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 653)
        Me.Controls.Add(Me.LblNombreJoueur)
        Me.Controls.Add(Me.pnlNbJoueurs)
        Me.Controls.Add(Me.GpbJoueur)
        Me.Controls.Add(Me.CmdJouer)
        Me.Controls.Add(Me.LblRegle)
        Me.Controls.Add(Me.PtbTitre)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(800, 700)
        Me.MinimumSize = New System.Drawing.Size(800, 700)
        Me.Name = "FrmParametre"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paramètre de la partie"
        Me.pnlNbJoueurs.ResumeLayout(False)
        Me.pnlNbJoueurs.PerformLayout()
        Me.GpbJoueur.ResumeLayout(False)
        Me.PnlJ4.ResumeLayout(False)
        Me.PnlJ4.PerformLayout()
        Me.PnlJ3.ResumeLayout(False)
        Me.PnlJ3.PerformLayout()
        Me.PnlJ2.ResumeLayout(False)
        Me.PnlJ2.PerformLayout()
        Me.PnlJ1.ResumeLayout(False)
        Me.PnlJ1.PerformLayout()
        CType(Me.PtbTitre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnlNbJoueurs As Panel
    Friend WithEvents LblNombreJoueur As Label
    Friend WithEvents RbtDeux As RadioButton
    Friend WithEvents RbtTrois As RadioButton
    Friend WithEvents RbtQuatre As RadioButton
    Friend WithEvents TxtNomJ2 As TextBox
    Friend WithEvents DtpJ2 As DateTimePicker
    Friend WithEvents TxtNomJ3 As TextBox
    Friend WithEvents DtpJ3 As DateTimePicker
    Friend WithEvents TxtNomJ4 As TextBox
    Friend WithEvents DtpJ4 As DateTimePicker
    Friend WithEvents tltParametres As ToolTip
    Friend WithEvents GpbJoueur As GroupBox
    Friend WithEvents PnlJ4 As Panel
    Friend WithEvents PnlJ3 As Panel
    Friend WithEvents PnlJ2 As Panel
    Friend WithEvents PnlJ1 As Panel
    Friend WithEvents TxtNomJ1 As TextBox
    Friend WithEvents DtpJ1 As DateTimePicker
    Friend WithEvents CmdJouer As Button
    Friend WithEvents LblRegle As LinkLabel
    Friend WithEvents PtbTitre As PictureBox
End Class
