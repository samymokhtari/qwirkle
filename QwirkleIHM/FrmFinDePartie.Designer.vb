﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFinDePartie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblNomJoueurVictoire = New System.Windows.Forms.Label()
        Me.LblDescVictoire = New System.Windows.Forms.Label()
        Me.LblFinPartie = New System.Windows.Forms.Label()
        Me.LblPoint3 = New System.Windows.Forms.Label()
        Me.LblNomJoueur1 = New System.Windows.Forms.Label()
        Me.LblPoint4 = New System.Windows.Forms.Label()
        Me.LblNomJoueur2 = New System.Windows.Forms.Label()
        Me.LblNomJoueur3 = New System.Windows.Forms.Label()
        Me.LblPoint2 = New System.Windows.Forms.Label()
        Me.LblNomJoueur4 = New System.Windows.Forms.Label()
        Me.LblPoint1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PnlPointJ1 = New System.Windows.Forms.Panel()
        Me.PnlPointJ4 = New System.Windows.Forms.Panel()
        Me.BtnQuitter = New System.Windows.Forms.Button()
        Me.PnlPointJ3 = New System.Windows.Forms.Panel()
        Me.PnlPointJ2 = New System.Windows.Forms.Panel()
        Me.GroupBox1.SuspendLayout()
        Me.PnlPointJ1.SuspendLayout()
        Me.PnlPointJ4.SuspendLayout()
        Me.PnlPointJ3.SuspendLayout()
        Me.PnlPointJ2.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblNomJoueurVictoire
        '
        Me.LblNomJoueurVictoire.AutoSize = True
        Me.LblNomJoueurVictoire.Font = New System.Drawing.Font("Papyrus", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomJoueurVictoire.ForeColor = System.Drawing.Color.Red
        Me.LblNomJoueurVictoire.Location = New System.Drawing.Point(443, 111)
        Me.LblNomJoueurVictoire.Name = "LblNomJoueurVictoire"
        Me.LblNomJoueurVictoire.Size = New System.Drawing.Size(62, 33)
        Me.LblNomJoueurVictoire.TabIndex = 16
        Me.LblNomJoueurVictoire.Text = "<val>"
        '
        'LblDescVictoire
        '
        Me.LblDescVictoire.AutoSize = True
        Me.LblDescVictoire.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescVictoire.Location = New System.Drawing.Point(292, 117)
        Me.LblDescVictoire.Name = "LblDescVictoire"
        Me.LblDescVictoire.Size = New System.Drawing.Size(155, 20)
        Me.LblDescVictoire.TabIndex = 15
        Me.LblDescVictoire.Text = "La victoire reviens à :"
        '
        'LblFinPartie
        '
        Me.LblFinPartie.AutoSize = True
        Me.LblFinPartie.Font = New System.Drawing.Font("Modern No. 20", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFinPartie.Location = New System.Drawing.Point(290, 27)
        Me.LblFinPartie.Name = "LblFinPartie"
        Me.LblFinPartie.Size = New System.Drawing.Size(236, 34)
        Me.LblFinPartie.TabIndex = 14
        Me.LblFinPartie.Text = "Fin de la partie"
        '
        'LblPoint3
        '
        Me.LblPoint3.AutoSize = True
        Me.LblPoint3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblPoint3.Location = New System.Drawing.Point(129, 5)
        Me.LblPoint3.Name = "LblPoint3"
        Me.LblPoint3.Size = New System.Drawing.Size(40, 17)
        Me.LblPoint3.TabIndex = 10
        Me.LblPoint3.Text = "Point"
        '
        'LblNomJoueur1
        '
        Me.LblNomJoueur1.AutoSize = True
        Me.LblNomJoueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblNomJoueur1.Location = New System.Drawing.Point(37, 4)
        Me.LblNomJoueur1.Name = "LblNomJoueur1"
        Me.LblNomJoueur1.Size = New System.Drawing.Size(86, 17)
        Me.LblNomJoueur1.TabIndex = 4
        Me.LblNomJoueur1.Text = "Joueur n°1 :"
        '
        'LblPoint4
        '
        Me.LblPoint4.AutoSize = True
        Me.LblPoint4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblPoint4.Location = New System.Drawing.Point(129, 5)
        Me.LblPoint4.Name = "LblPoint4"
        Me.LblPoint4.Size = New System.Drawing.Size(40, 17)
        Me.LblPoint4.TabIndex = 11
        Me.LblPoint4.Text = "Point"
        '
        'LblNomJoueur2
        '
        Me.LblNomJoueur2.AutoSize = True
        Me.LblNomJoueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblNomJoueur2.Location = New System.Drawing.Point(37, 4)
        Me.LblNomJoueur2.Name = "LblNomJoueur2"
        Me.LblNomJoueur2.Size = New System.Drawing.Size(86, 17)
        Me.LblNomJoueur2.TabIndex = 5
        Me.LblNomJoueur2.Text = "Joueur n°2 :"
        '
        'LblNomJoueur3
        '
        Me.LblNomJoueur3.AutoSize = True
        Me.LblNomJoueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblNomJoueur3.Location = New System.Drawing.Point(37, 5)
        Me.LblNomJoueur3.Name = "LblNomJoueur3"
        Me.LblNomJoueur3.Size = New System.Drawing.Size(86, 17)
        Me.LblNomJoueur3.TabIndex = 6
        Me.LblNomJoueur3.Text = "Joueur n°3 :"
        '
        'LblPoint2
        '
        Me.LblPoint2.AutoSize = True
        Me.LblPoint2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblPoint2.Location = New System.Drawing.Point(129, 4)
        Me.LblPoint2.Name = "LblPoint2"
        Me.LblPoint2.Size = New System.Drawing.Size(40, 17)
        Me.LblPoint2.TabIndex = 9
        Me.LblPoint2.Text = "Point"
        '
        'LblNomJoueur4
        '
        Me.LblNomJoueur4.AutoSize = True
        Me.LblNomJoueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblNomJoueur4.Location = New System.Drawing.Point(37, 5)
        Me.LblNomJoueur4.Name = "LblNomJoueur4"
        Me.LblNomJoueur4.Size = New System.Drawing.Size(86, 17)
        Me.LblNomJoueur4.TabIndex = 7
        Me.LblNomJoueur4.Text = "Joueur n°4 :"
        '
        'LblPoint1
        '
        Me.LblPoint1.AutoSize = True
        Me.LblPoint1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.LblPoint1.Location = New System.Drawing.Point(129, 4)
        Me.LblPoint1.Name = "LblPoint1"
        Me.LblPoint1.Size = New System.Drawing.Size(40, 17)
        Me.LblPoint1.TabIndex = 8
        Me.LblPoint1.Text = "Point"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.PnlPointJ1)
        Me.GroupBox1.Controls.Add(Me.PnlPointJ4)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(296, 171)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(209, 125)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tableau des scores"
        '
        'PnlPointJ1
        '
        Me.PnlPointJ1.Controls.Add(Me.LblPoint1)
        Me.PnlPointJ1.Controls.Add(Me.LblNomJoueur1)
        Me.PnlPointJ1.Location = New System.Drawing.Point(0, 26)
        Me.PnlPointJ1.Name = "PnlPointJ1"
        Me.PnlPointJ1.Size = New System.Drawing.Size(209, 26)
        Me.PnlPointJ1.TabIndex = 0
        '
        'PnlPointJ4
        '
        Me.PnlPointJ4.Controls.Add(Me.LblNomJoueur4)
        Me.PnlPointJ4.Controls.Add(Me.LblPoint4)
        Me.PnlPointJ4.Location = New System.Drawing.Point(0, 98)
        Me.PnlPointJ4.Name = "PnlPointJ4"
        Me.PnlPointJ4.Size = New System.Drawing.Size(209, 27)
        Me.PnlPointJ4.TabIndex = 19
        '
        'BtnQuitter
        '
        Me.BtnQuitter.AutoSize = True
        Me.BtnQuitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.5!)
        Me.BtnQuitter.Location = New System.Drawing.Point(336, 336)
        Me.BtnQuitter.Name = "BtnQuitter"
        Me.BtnQuitter.Size = New System.Drawing.Size(124, 42)
        Me.BtnQuitter.TabIndex = 17
        Me.BtnQuitter.Text = "Quitter"
        Me.BtnQuitter.UseVisualStyleBackColor = True
        '
        'PnlPointJ3
        '
        Me.PnlPointJ3.Controls.Add(Me.LblNomJoueur3)
        Me.PnlPointJ3.Controls.Add(Me.LblPoint3)
        Me.PnlPointJ3.Location = New System.Drawing.Point(296, 245)
        Me.PnlPointJ3.Name = "PnlPointJ3"
        Me.PnlPointJ3.Size = New System.Drawing.Size(209, 26)
        Me.PnlPointJ3.TabIndex = 1
        '
        'PnlPointJ2
        '
        Me.PnlPointJ2.Controls.Add(Me.LblNomJoueur2)
        Me.PnlPointJ2.Controls.Add(Me.LblPoint2)
        Me.PnlPointJ2.Location = New System.Drawing.Point(296, 221)
        Me.PnlPointJ2.Name = "PnlPointJ2"
        Me.PnlPointJ2.Size = New System.Drawing.Size(209, 26)
        Me.PnlPointJ2.TabIndex = 20
        '
        'FrmFinDePartie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.PnlPointJ2)
        Me.Controls.Add(Me.PnlPointJ3)
        Me.Controls.Add(Me.LblNomJoueurVictoire)
        Me.Controls.Add(Me.LblDescVictoire)
        Me.Controls.Add(Me.LblFinPartie)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BtnQuitter)
        Me.Name = "FrmFinDePartie"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GroupBox1.ResumeLayout(False)
        Me.PnlPointJ1.ResumeLayout(False)
        Me.PnlPointJ1.PerformLayout()
        Me.PnlPointJ4.ResumeLayout(False)
        Me.PnlPointJ4.PerformLayout()
        Me.PnlPointJ3.ResumeLayout(False)
        Me.PnlPointJ3.PerformLayout()
        Me.PnlPointJ2.ResumeLayout(False)
        Me.PnlPointJ2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblNomJoueurVictoire As Label
    Friend WithEvents LblDescVictoire As Label
    Friend WithEvents LblFinPartie As Label
    Friend WithEvents LblPoint3 As Label
    Friend WithEvents LblNomJoueur1 As Label
    Friend WithEvents LblPoint4 As Label
    Friend WithEvents LblNomJoueur2 As Label
    Friend WithEvents LblNomJoueur3 As Label
    Friend WithEvents LblPoint2 As Label
    Friend WithEvents LblNomJoueur4 As Label
    Friend WithEvents LblPoint1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents BtnQuitter As Button
    Friend WithEvents PnlPointJ3 As Panel
    Friend WithEvents PnlPointJ1 As Panel
    Friend WithEvents PnlPointJ4 As Panel
    Friend WithEvents PnlPointJ2 As Panel
End Class
