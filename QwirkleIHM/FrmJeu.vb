﻿Imports Qwirkle
Imports System.Runtime.InteropServices
Imports System.Drawing.Bitmap
Imports System.IO.StreamReader
Imports System.IO
'afficher les tags quand on passe la souris sur les pictures box
Public Class FrmJeu
    Dim J1 As Joueur = New Joueur(FrmParametre.TxtNomJ1.Text, FrmParametre.DtpJ1.Value.Day, FrmParametre.DtpJ1.Value.Month, FrmParametre.DtpJ1.Value.Year)
    Dim J2 As Joueur = New Joueur(FrmParametre.TxtNomJ2.Text, FrmParametre.DtpJ2.Value.Day, FrmParametre.DtpJ2.Value.Month, FrmParametre.DtpJ2.Value.Year)
    Dim J3 As Joueur = New Joueur(FrmParametre.TxtNomJ3.Text, FrmParametre.DtpJ3.Value.Day, FrmParametre.DtpJ3.Value.Month, FrmParametre.DtpJ3.Value.Year)
    Dim J4 As Joueur = New Joueur(FrmParametre.TxtNomJ4.Text, FrmParametre.DtpJ4.Value.Day, FrmParametre.DtpJ4.Value.Month, FrmParametre.DtpJ4.Value.Year)
    Dim listeJoueurs As New List(Of Joueur)
    Dim tuileEnJeu As Tuile
    Dim tuilesAdjacente As List(Of Tuile) = New List(Of Tuile)
    Dim reroll As Boolean = False, jouerplateau As Boolean = True
    Dim listeTuileEnJeu As List(Of Tuile) = New List(Of Tuile)
    Dim listeAdjColonne As List(Of Tuile) = New List(Of Tuile)
    Dim listeAdjLigne As List(Of Tuile) = New List(Of Tuile)
    Dim listeMots As List(Of List(Of Tuile)) = New List(Of List(Of Tuile))
    Dim nombreJoueurs As Integer
    Dim tourJoueur As Integer = 0
    Public partie As Partie
    Dim pioche As Pioche = New Pioche()
    Dim indexJoueurActuel
    Dim minLigne, minColonne, maxLigne, maxColonne
    Dim colDerniereTuile, ligDerniereTuile As Integer
    Dim premTuile As Boolean = True
    Dim nbTuileposer As Integer
    Dim verrColonne = False, verrLigne = False
    Dim positionDerniereTuile
    Dim tuilesJouees As New List(Of PictureBox)
    Dim PremierTour As Boolean = True
    Private Const APPCOMMAND_VOLUME_MUTE As Integer = &H80000
    Private Const WM_APPCOMMAND As Integer = &H319


    <DllImport("user32.dll")> Public Shared Function SendMessageW(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    End Function
    Sub PlayLoopingBackgroundSoundFile() 'Musique en jeu
        My.Computer.Audio.Play(My.Resources.qwirkle_jeu,
        AudioPlayMode.BackgroundLoop)
    End Sub

    'Premier parcours du plateau qui initialise toutes les pictureBox du tableau en mettant les allowdrop a true 
    Public Sub InitialisationPlateau()
        pioche = New Pioche()
        minLigne = 0
        minColonne = 0
        maxLigne = 2
        maxColonne = 2
        For i As Byte = minLigne To maxLigne
            For j As Byte = minColonne To maxColonne
                Dim pic As New PictureBox With {
                    .Name = "ptb" & i.ToString() & j.ToString(),
                    .AllowDrop = True,
                    .Tag = i.ToString() & j.ToString(),
                    .Size = New Drawing.Size(30, 30),
                    .SizeMode = PictureBoxSizeMode.StretchImage
                }
                TlpPlateau.Controls.Add(pic, j, i)
                AddHandler pic.DragEnter, AddressOf PtbTuile_DragEnter
                AddHandler pic.DragDrop, AddressOf TlpJeu_DragDrop
            Next
        Next
        PtbEchangeTuile.AllowDrop = True
    End Sub

    '
    Public Sub InitialisationJoueurs()
        listeJoueurs.Add(J1)
        listeJoueurs.Add(J2)
        listeJoueurs.Add(J3)
        listeJoueurs.Add(J4)
        For i As Byte = 0 To nombreJoueurs - 1
            partie.AjouterJoueur(i, listeJoueurs.Item(i).Nom, listeJoueurs.Item(i).JourNais, listeJoueurs.Item(i).MoisNais, listeJoueurs.Item(i).AnneeNais)
            partie.GetJoueurOf(i).RemplirMain(pioche)
        Next
        listeJoueurs.RemoveRange(nombreJoueurs, 4 - nombreJoueurs)
    End Sub

    'Mise à jour des tags des tuiles dans le plateau pour les agrandissements tu tableau à gauche et en haut
    Public Sub MajTag()
        For Each pic As PictureBox In TlpPlateau.Controls
            pic.Tag = TlpPlateau.GetRow(pic).ToString() & TlpPlateau.GetColumn(pic).ToString()
        Next
    End Sub

    'Parcours du tableau pour la verification des allowdrops de chaque picturebox en fonction des compatibilités des tuiles et du posage en ligne et colonnes
    Public Sub MajDispoCase()
        Dim col
        Dim lig
        Dim coordonnee

        For Each pic As PictureBox In TlpPlateau.Controls
            pic.AllowDrop = False
        Next
        For Each pic As PictureBox In TlpPlateau.Controls
            If (pic.Image IsNot Nothing) Then
                col = TlpPlateau.GetColumn(pic)
                lig = TlpPlateau.GetRow(pic)
                coordonnee = col + (10 * lig)
                For Each pic2 As PictureBox In TlpPlateau.Controls
                    If (pic2.Tag = coordonnee - 1 Or pic2.Tag = coordonnee + 1 Or pic2.Tag = coordonnee + 10 Or pic2.Tag = coordonnee - 10) Then
                        If (pic2.Image Is Nothing) Then
                            pic2.AllowDrop = True
                        End If
                    End If
                Next
            End If
        Next
    End Sub


    'Méthodes de mise à jour des colonnes et des tuiles en fonction de l'agrandissement sur les différents cotés du tableau
    Public Sub MajPlateauColonneMax()

        For i As Byte = minLigne To maxLigne
            Dim pic As New PictureBox With {
                    .Name = "ptb" & i.ToString() & maxColonne.ToString(),
                    .Tag = i.ToString() & maxColonne.ToString(),
                    .SizeMode = PictureBoxSizeMode.StretchImage,
                    .Size = New Size(30, 30)
                }
            TlpPlateau.Controls.Add(pic, maxColonne, i)
            AddHandler pic.DragEnter, AddressOf PtbTuile_DragEnter
            AddHandler pic.DragDrop, AddressOf TlpJeu_DragDrop
        Next
    End Sub
    Public Sub MajPlateauLigneMax()

        For i As Byte = minColonne To maxColonne
            Dim pic As New PictureBox With {
                    .Name = "ptb" & maxLigne.ToString() & i.ToString(),
                    .Tag = maxLigne.ToString() & i.ToString(),
                    .SizeMode = PictureBoxSizeMode.StretchImage,
                    .Size = New Size(30, 30)
                }
            TlpPlateau.Controls.Add(pic, i, maxLigne)
            AddHandler pic.DragEnter, AddressOf PtbTuile_DragEnter
            AddHandler pic.DragDrop, AddressOf TlpJeu_DragDrop
        Next
    End Sub
    Public Sub MajPlateauColonneMin()

        For i As Byte = minLigne To maxLigne
            Dim pic As New PictureBox With {
                .Name = "ptb" & i.ToString() & minColonne.ToString(),
                .Tag = i.ToString() & minColonne.ToString(),
                .SizeMode = PictureBoxSizeMode.StretchImage,
                .Size = New Size(30, 30)
            }
            TlpPlateau.Controls.Add(pic, minColonne, i)
            AddHandler pic.DragEnter, AddressOf PtbTuile_DragEnter
            AddHandler pic.DragDrop, AddressOf TlpJeu_DragDrop
        Next

    End Sub
    Public Sub MajPlateauLigneMin()

        For i As Byte = minColonne To maxColonne
            Dim pic As New PictureBox With {
                .Name = "ptb" & minLigne.ToString() & i.ToString(),
                .Tag = minColonne.ToString() & i.ToString(),
                .SizeMode = PictureBoxSizeMode.StretchImage,
                .Size = New Size(30, 30)
            }
            TlpPlateau.Controls.Add(pic, i, minLigne)
            AddHandler pic.DragEnter, AddressOf PtbTuile_DragEnter
            AddHandler pic.DragDrop, AddressOf TlpJeu_DragDrop
        Next

    End Sub

    'décalage des tuiles sur le tableau si une tuile est placée en haut ou à gauche
    Public Sub DecalagePlateauColonne()
        For Each p As PictureBox In TlpPlateau.Controls
            'Si l'image n'est pas nothing, on la déplace à bas
            TlpPlateau.SetColumn(p, TlpPlateau.GetColumn(p) + 1)
        Next
    End Sub
    Public Sub DecalagePlateauLigne()
        For Each p As PictureBox In TlpPlateau.Controls
            'Si l'image n'est pas nothing, on la déplace en bas
            TlpPlateau.SetRow(p, TlpPlateau.GetRow(p) + 1)
        Next
    End Sub

    'Agrandissement du tableau si une tuile est posée sur une extremité du plateau
    Public Sub MajTaillePlateau(p_image As PictureBox)
        Dim c As New ColumnStyle
        Dim r As New RowStyle

        TlpPlateau.SuspendLayout()

        'ajout d'une tuile à droite
        If (TlpPlateau.GetColumn(p_image) = maxColonne) Then
            TlpPlateau.ColumnCount += 1
            c.SizeType = SizeType.AutoSize
            TlpPlateau.ColumnStyles.Add(c)
            maxColonne += 1
            MajPlateauColonneMax()
        End If
        'ajout d'une tuile en bas
        If (TlpPlateau.GetRow(p_image) = maxLigne) Then
            TlpPlateau.RowCount += 1
            r.SizeType = SizeType.AutoSize
            TlpPlateau.RowStyles.Add(r)
            maxLigne += 1
            MajPlateauLigneMax()
        End If
        'ajout d'une tuile à gauche
        If (TlpPlateau.GetColumn(p_image) = minColonne) Then
            TlpPlateau.ColumnCount += 1
            c.SizeType = SizeType.AutoSize
            TlpPlateau.ColumnStyles.Insert(minColonne, c)
            DecalagePlateauColonne()
            maxColonne += 1
            MajPlateauColonneMin()
        End If
        'ajout d'une tuile en haut
        If (TlpPlateau.GetRow(p_image) = minLigne) Then
            TlpPlateau.RowCount += 1
            r.SizeType = SizeType.AutoSize
            TlpPlateau.RowStyles.Insert(minLigne, r)
            DecalagePlateauLigne()
            maxLigne += 1
            MajPlateauLigneMin()
        End If

        TlpPlateau.ResumeLayout()
    End Sub

    'Mise à jour des points des joueurs et des tuiles restatntes dans la pioche
    Public Sub MajInfo()
        LblPointJ1.Text = partie.GetJoueurOf(0).Point
        LblPointJ2.Text = partie.GetJoueurOf(1).Point
        LblNbTuileRestante.Text = pioche.CountTuiles
        LblTourJoueur.Text = partie.GetJoueurOf(indexJoueurActuel).Nom.ToUpper

        If (partie.NbJoueur > 2) Then
            LblPointJ3.Text = partie.GetJoueurOf(2).Point
            If (partie.NbJoueur > 3) Then
                LblPointJ4.Text = partie.GetJoueurOf(3).Point
            End If
        End If
        LblPointJ1.Text = partie.GetJoueurOf(0).Point
        LblPointJ2.Text = partie.GetJoueurOf(1).Point
    End Sub

    'Affiche chaque tuile da la main du joueur dans l'endroit prévu à cet effet
    Public Sub AfficherMainJoueur(j As Joueur, pnl As Panel)
        Dim i As Byte = 0
        For Each pic As PictureBox In pnl.Controls
            pic.ImageLocation = j.RecupererTuile(i).Image
            pic.Name = "ptb-" + j.RecupererTuile(i).Forme + "-" + j.RecupererTuile(i).Couleur
            pic.Tag = i.ToString
            i = i + 1
        Next
    End Sub

    'Cette fonction prends une picturebox en paramètre et créer une tuile depuis celle-ci
    Function ConvertisseurPictureBoxToTuile(p As PictureBox)
        Dim stringReader, f, c As String
        stringReader = p.Name
        Dim words As String() = stringReader.Split(New Char() {"-"})
        f = words(1)
        c = words(2)
        Return New Tuile(f, c)
    End Function

    Public Sub StockageImageAdjacenteLigne(p As PictureBox, listT As List(Of Tuile))
        Dim img As PictureBox = New PictureBox
        For ind As Byte = minColonne To maxColonne
            img = TlpPlateau.GetControlFromPosition(ind, TlpPlateau.GetRow(p))
            If (img.Image IsNot Nothing And img.Image IsNot p.Image) Then
                listT.Add(ConvertisseurPictureBoxToTuile(img))
            End If
        Next
    End Sub
    Public Sub StockageImageAdjacenteColonne(p As PictureBox, listT As List(Of Tuile))
        Dim img As PictureBox = New PictureBox
        For ind As Byte = minLigne To maxLigne
            img = TlpPlateau.GetControlFromPosition(TlpPlateau.GetColumn(p), ind)
            If (img.Image IsNot Nothing And img.Image IsNot p.Image) Then
                listT.Add(ConvertisseurPictureBoxToTuile(img))
            End If
        Next
    End Sub

    Public Sub RecuperationTuileColonne(p As PictureBox, listT As List(Of Tuile))
        Dim img As PictureBox = New PictureBox
        For ind As Byte = minLigne To maxLigne
            img = TlpPlateau.GetControlFromPosition(TlpPlateau.GetColumn(p), ind)
            If (img.Image IsNot Nothing) Then
                listT.Add(ConvertisseurPictureBoxToTuile(img))
            End If
        Next
    End Sub
    Public Sub RecuperationTuileLigne(p As PictureBox, listT As List(Of Tuile))
        Dim img As PictureBox = New PictureBox
        For ind As Byte = minColonne To maxColonne
            img = TlpPlateau.GetControlFromPosition(ind, TlpPlateau.GetRow(p))
            If (img.Image IsNot Nothing) Then
                listT.Add(ConvertisseurPictureBoxToTuile(img))
            End If
        Next
    End Sub


    'Maj des variables en fin de tour
    Public Sub FinDeTour()
        pnlMainJoueur.Hide()
        MessageBox.Show("Continuer", "Tour suivant", MessageBoxButtons.OK)
        listeTuileEnJeu.Clear()
        AfficherMainJoueur(partie.GetJoueurOf(indexJoueurActuel), pnlMainJoueur)
        pnlMainJoueur.Show()

        PtbEchangeTuile.BackColor = Color.Red
        lblReroll.Text = ""
        reroll = True
        jouerplateau = True
        premTuile = True
        nbTuileposer = 0
        colDerniereTuile = -1
        ligDerniereTuile = -1
        verrColonne = False
        verrLigne = False
        PremierTour = False
        tuilesJouees.Clear()
        listeAdjColonne.Clear()
        listeAdjLigne.Clear()
        listeMots.Clear()
        MajInfo()
        MajTag()
        MajDispoCase()
        BtnValider.Enabled = False
    End Sub

    'Premier chargement de la fenêtre du jeu
    Private Sub EcranJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load 'Chargement du formulaire
        TlpPlateau.MaximumSize = New Size(Size.Height - 350, Size.Width - 450)
        PlayLoopingBackgroundSoundFile()
        indexJoueurActuel = 0
        colDerniereTuile = -1
        ligDerniereTuile = -1
        For Each Radio As RadioButton In FrmParametre.pnlNbJoueurs.Controls
            If Radio.Checked = True Then
                nombreJoueurs = CInt(Radio.Tag)
            End If
        Next
        For Each pnl As Panel In gbxPointJoueurs.Controls
            If pnl.Tag > nombreJoueurs Then
                pnl.Hide()
            End If
        Next
        partie = New Partie(nombreJoueurs)

        LblNJ1.Text = J1.Nom
        LblNJ2.Text = J2.Nom
        LblNJ3.Text = J3.Nom
        LblNJ4.Text = J4.Nom
        InitialisationPlateau()
        InitialisationJoueurs()
        FrmSaisieMots.ShowDialog()
        partie.InitiliasationOrdreJoueur(FrmSaisieMots.ListeMot1)
        AfficherMainJoueur(partie.GetJoueurOf(indexJoueurActuel), pnlMainJoueur)
        MajInfo()
        BtnValider.Enabled = False
    End Sub

    'déplacement d'une image au clic gauche de la souris
    Private Sub PtbTuile_MouseMove(sender As Object, e As MouseEventArgs) Handles PtbTuile1.MouseMove, PtbTuile2.MouseMove, PtbTuile3.MouseMove, PtbTuile4.MouseMove, PtbTuile5.MouseMove, PtbTuile6.MouseMove
        Dim J = partie.GetJoueurOf(indexJoueurActuel)
        Dim emplacementTuile As PictureBox = sender
        If e.Button = MouseButtons.Left Then
            If emplacementTuile.Image IsNot Nothing Then
                Dim dragAndDrop As DragDropEffects
                dragAndDrop = emplacementTuile.DoDragDrop(emplacementTuile.Image, DragDropEffects.Move)
                If dragAndDrop = DragDropEffects.Move Then
                    emplacementTuile.Image = Nothing
                End If
            End If
        End If
        tuileEnJeu = ConvertisseurPictureBoxToTuile(sender)
    End Sub

    'zone d'arrivée du drag and drop
    Private Sub PtbTuile_DragEnter(sender As Object, e As DragEventArgs) Handles PtbEchangeTuile.DragEnter, PtbTuile1.DragEnter, PtbTuile2.DragEnter, PtbTuile3.DragEnter, PtbTuile4.DragEnter, PtbTuile5.DragEnter, PtbTuile6.DragEnter
        e.Effect = DragDropEffects.None
        Dim emplacementTuile As PictureBox = sender

        If (sender IsNot PtbEchangeTuile And jouerplateau = True) Then
            StockageImageAdjacenteLigne(sender, tuilesAdjacente)
            StockageImageAdjacenteColonne(sender, tuilesAdjacente)

            If (e.Data.GetDataPresent(DataFormats.Bitmap)) And (tuileEnJeu.Compatible(tuilesAdjacente) = True) Then
                If (verrLigne = True And TlpPlateau.GetRow(sender) = ligDerniereTuile) Or premTuile = True Then
                    e.Effect = DragDropEffects.Move
                End If
                If (verrColonne = True And TlpPlateau.GetColumn(sender) = colDerniereTuile) Then
                    e.Effect = DragDropEffects.Move
                End If
                If (nbTuileposer = 1) Then
                    e.Effect = DragDropEffects.Move
                End If
            End If
        End If

        If (e.Data.GetDataPresent(DataFormats.Bitmap)) And (sender Is PtbEchangeTuile) And (reroll = True) Then
            e.Effect = DragDropEffects.Move
        End If
        tuilesAdjacente.Clear()
    End Sub

    'zone de départ du drag and drop
    Private Sub TlpJeu_DragDrop(sender As Object, e As DragEventArgs) Handles TlpPlateau.DragDrop, PtbEchangeTuile.DragDrop
        Dim J = partie.GetJoueurOf(indexJoueurActuel)
        Dim emplacementTuile As PictureBox = sender
        If (emplacementTuile Is PtbEchangeTuile) Then
            jouerplateau = False
            listeTuileEnJeu.Add(tuileEnJeu)
            PtbEchangeTuile.BackColor = Color.Green
            PtbEchangeTuile.Image = Nothing
            lblReroll.Text = "REROLL"
            tuilesJouees.Add(emplacementTuile)
        Else
            reroll = False
            premTuile = False
            emplacementTuile.Image = e.Data.GetData(DataFormats.Bitmap)
            emplacementTuile.Name = emplacementTuile.Name + "-" + tuileEnJeu.Forme + "-" + tuileEnJeu.Couleur
            MajTaillePlateau(emplacementTuile)
            nbTuileposer = nbTuileposer + 1
            tuilesJouees.Add(emplacementTuile)
            positionDerniereTuile = TlpPlateau.Controls.GetChildIndex(emplacementTuile)
            MajTag()
            MajDispoCase()
            reroll = False

            If (nbTuileposer < 3) Then
                If (ligDerniereTuile = TlpPlateau.GetRow(emplacementTuile)) Then
                    verrLigne = True
                End If
                If (colDerniereTuile = TlpPlateau.GetColumn(emplacementTuile)) Then
                    verrColonne = True
                End If
                colDerniereTuile = TlpPlateau.GetColumn(emplacementTuile)
                ligDerniereTuile = TlpPlateau.GetRow(emplacementTuile)
            End If

            If (nbTuileposer = 1) Then
                For Each p As PictureBox In TlpPlateau.Controls
                    If (p.Tag = emplacementTuile.Tag + 1 Or p.Tag = emplacementTuile.Tag + 10 Or p.Tag = emplacementTuile.Tag - 1 Or p.Tag = emplacementTuile.Tag - 10) Then
                        p.AllowDrop = True
                    Else
                        p.AllowDrop = False
                    End If
                Next
            End If
        End If
        BtnValider.Enabled = True
    End Sub

    'Bouton qui mute la musique ou la demute
    Private Sub CmdMute_Click(sender As Object, e As EventArgs) Handles cmdMute.Click 'Bouton pour gérer le son
        SendMessageW(Me.Handle, WM_APPCOMMAND, Me.Handle, New IntPtr(APPCOMMAND_VOLUME_MUTE))
        If (cmdMute.Text = "🔇") Then
            cmdMute.BackColor = Color.Azure
            cmdMute.Text = "🔊"
        Else
            cmdMute.Text = "🔇"
            cmdMute.BackColor = Color.Red
        End If
    End Sub

    'Bouton qui valide le tour du joueur actuel en remplissant sa main avec les tuiles dispo dans la pioche et qui passe au joueur suivant
    Private Sub BtnValider_Click(sender As Object, e As EventArgs) Handles BtnValider.Click
        Dim J = partie.GetJoueurOf(indexJoueurActuel)
        If (reroll = True) Then
            J.FinDeTour(pioche, listeTuileEnJeu)

        Else
            For Each p As PictureBox In tuilesJouees
                J.Joue(ConvertisseurPictureBoxToTuile(p))
            Next
            RecuperationTuileColonne(tuilesJouees.First, listeAdjColonne)
            RecuperationTuileLigne(tuilesJouees.First, listeAdjLigne)
            listeMots.Add(listeAdjColonne)
            listeMots.Add(listeAdjLigne)
            J.FinDeTour(pioche, listeMots)
        End If
        If indexJoueurActuel = partie.NbJoueur - 1 Then
            indexJoueurActuel = 0
        Else
            indexJoueurActuel = indexJoueurActuel + 1
        End If

        'Masquage de la main courante et affichage d'une boite de message permettant de continuer vers le tour suivant
        FinDeTour()
    End Sub

    'bouton qui annule tous les mouvements du joueur durant son tour
    Private Sub BtnAnuller_Click(sender As Object, e As EventArgs) Handles BtnAnuller.Click

        For Each picMain As Control In Me.pnlMainJoueur.Controls
            If CType(picMain, PictureBox).Image Is Nothing Then
                CType(picMain, PictureBox).Image = tuilesJouees(0).Image
                tuilesJouees(0).Image = Nothing
                tuilesJouees.RemoveAt(0)
            End If
        Next
        If PremierTour = True Then
            For Each pic As PictureBox In TlpPlateau.Controls
                pic.AllowDrop = True
            Next
            reroll = False
        Else
            reroll = True
            MajTag()
            MajDispoCase()
        End If
        listeTuileEnJeu.Clear()
        MajInfo()
        AfficherMainJoueur(partie.GetJoueurOf(indexJoueurActuel), pnlMainJoueur)
        nbTuileposer = 0
        premTuile = True
        verrLigne = False
        verrColonne = False
        jouerplateau = True
        PtbEchangeTuile.BackColor = Color.Red
        lblReroll.Text = ""
        BtnValider.Enabled = False
    End Sub
End Class
