﻿Imports Qwirkle

Public Class FrmParametre
    Private Sub LblRegle_MouseDown(sender As Object, e As MouseEventArgs) Handles LblRegle.MouseDown
        System.Diagnostics.Process.Start("https://ent.iut-amiens.fr/pluginfile.php/32619/mod_resource/content/1/Qwirkle_regles_FR_web.pdf")
    End Sub

    Sub PlayLoopingBackgroundSoundFile()
        My.Computer.Audio.Play(My.Resources.qwirkle_parametre,
        AudioPlayMode.BackgroundLoop)
    End Sub

    Private Sub FrmParametre_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RbtDeux.Checked = True
        PlayLoopingBackgroundSoundFile()
        PnlJ3.Hide()
        PnlJ4.Hide()
    End Sub

    Private Sub RbtDeux_Click(sender As Object, e As EventArgs) Handles RbtDeux.Click
        PnlJ3.Hide()
        PnlJ4.Hide()
    End Sub

    Private Sub RbtTrois_Click(sender As Object, e As EventArgs) Handles RbtTrois.Click
        PnlJ3.Show()
        PnlJ4.Hide()
    End Sub

    Private Sub RbtQuatre_Click(sender As Object, e As EventArgs) Handles RbtQuatre.Click
        PnlJ3.Show()
        PnlJ4.Show()
    End Sub

    Private Sub TxtNomJ1_MouseEnter(sender As Object, e As EventArgs) Handles TxtNomJ1.MouseEnter, TxtNomJ2.MouseEnter, TxtNomJ3.MouseEnter, TxtNomJ4.MouseEnter
        tltParametres.Show("Saisissez le nom du joueur", sender)
    End Sub

    Private Sub DtpJ1_MouseEnter(sender As Object, e As EventArgs) Handles DtpJ1.MouseEnter, DtpJ2.MouseEnter, DtpJ3.MouseEnter, DtpJ4.MouseEnter
        tltParametres.Show("Saisissez la date de naissance", sender)
    End Sub

    Private Sub CmdJouer_Click(sender As Object, e As EventArgs) Handles CmdJouer.Click
        FrmJeu.Show()
        Me.Close()

    End Sub
End Class
