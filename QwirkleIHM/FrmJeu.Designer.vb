﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmJeu))
        Me.LblNJ2 = New System.Windows.Forms.Label()
        Me.LblPointJ2 = New System.Windows.Forms.Label()
        Me.PnlPointJ2 = New System.Windows.Forms.Panel()
        Me.LblNJ3 = New System.Windows.Forms.Label()
        Me.LblPointJ3 = New System.Windows.Forms.Label()
        Me.PnlPointJ3 = New System.Windows.Forms.Panel()
        Me.LblNJ4 = New System.Windows.Forms.Label()
        Me.LblPointJ4 = New System.Windows.Forms.Label()
        Me.PnlPointJ4 = New System.Windows.Forms.Panel()
        Me.TlpPlateau = New System.Windows.Forms.TableLayoutPanel()
        Me.LblTourJoueur = New System.Windows.Forms.Label()
        Me.PnlPointJ1 = New System.Windows.Forms.Panel()
        Me.LblNJ1 = New System.Windows.Forms.Label()
        Me.LblPointJ1 = New System.Windows.Forms.Label()
        Me.LblTour = New System.Windows.Forms.Label()
        Me.BtnValider = New System.Windows.Forms.Button()
        Me.gbxPointJoueurs = New System.Windows.Forms.GroupBox()
        Me.LblDescEchangeTuile = New System.Windows.Forms.Label()
        Me.LblNbTuileRestante = New System.Windows.Forms.Label()
        Me.LblDescNbTuile = New System.Windows.Forms.Label()
        Me.BtnAnuller = New System.Windows.Forms.Button()
        Me.cmdMute = New System.Windows.Forms.Button()
        Me.pnlMainJoueur = New System.Windows.Forms.Panel()
        Me.lblReroll = New System.Windows.Forms.Label()
        Me.pnlReroll = New System.Windows.Forms.Panel()
        Me.pnlPioche = New System.Windows.Forms.Panel()
        Me.pnlTour = New System.Windows.Forms.Panel()
        Me.PtbPioche = New System.Windows.Forms.PictureBox()
        Me.PtbEchangeTuile = New System.Windows.Forms.PictureBox()
        Me.PtbTuile1 = New System.Windows.Forms.PictureBox()
        Me.PtbTuile2 = New System.Windows.Forms.PictureBox()
        Me.PtbTuile6 = New System.Windows.Forms.PictureBox()
        Me.PtbTuile3 = New System.Windows.Forms.PictureBox()
        Me.PtbTuile5 = New System.Windows.Forms.PictureBox()
        Me.PtbTuile4 = New System.Windows.Forms.PictureBox()
        Me.PnlPointJ2.SuspendLayout()
        Me.PnlPointJ3.SuspendLayout()
        Me.PnlPointJ4.SuspendLayout()
        Me.PnlPointJ1.SuspendLayout()
        Me.gbxPointJoueurs.SuspendLayout()
        Me.pnlMainJoueur.SuspendLayout()
        Me.pnlReroll.SuspendLayout()
        Me.pnlPioche.SuspendLayout()
        Me.pnlTour.SuspendLayout()
        CType(Me.PtbPioche, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbEchangeTuile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbTuile4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblNJ2
        '
        Me.LblNJ2.AutoSize = True
        Me.LblNJ2.Font = New System.Drawing.Font("Corbel", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNJ2.Location = New System.Drawing.Point(3, 12)
        Me.LblNJ2.Name = "LblNJ2"
        Me.LblNJ2.Size = New System.Drawing.Size(99, 18)
        Me.LblNJ2.TabIndex = 1
        Me.LblNJ2.Text = "Nom joueur 2 :"
        '
        'LblPointJ2
        '
        Me.LblPointJ2.AutoSize = True
        Me.LblPointJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPointJ2.Location = New System.Drawing.Point(121, 12)
        Me.LblPointJ2.Name = "LblPointJ2"
        Me.LblPointJ2.Size = New System.Drawing.Size(73, 18)
        Me.LblPointJ2.TabIndex = 5
        Me.LblPointJ2.Text = "<valeur>"
        '
        'PnlPointJ2
        '
        Me.PnlPointJ2.Controls.Add(Me.LblNJ2)
        Me.PnlPointJ2.Controls.Add(Me.LblPointJ2)
        Me.PnlPointJ2.Location = New System.Drawing.Point(6, 69)
        Me.PnlPointJ2.Name = "PnlPointJ2"
        Me.PnlPointJ2.Size = New System.Drawing.Size(200, 31)
        Me.PnlPointJ2.TabIndex = 28
        Me.PnlPointJ2.Tag = "2"
        '
        'LblNJ3
        '
        Me.LblNJ3.AutoSize = True
        Me.LblNJ3.Font = New System.Drawing.Font("Corbel", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNJ3.Location = New System.Drawing.Point(3, 12)
        Me.LblNJ3.Name = "LblNJ3"
        Me.LblNJ3.Size = New System.Drawing.Size(98, 18)
        Me.LblNJ3.TabIndex = 1
        Me.LblNJ3.Text = "Nom joueur 3 :"
        '
        'LblPointJ3
        '
        Me.LblPointJ3.AutoSize = True
        Me.LblPointJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPointJ3.Location = New System.Drawing.Point(121, 12)
        Me.LblPointJ3.Name = "LblPointJ3"
        Me.LblPointJ3.Size = New System.Drawing.Size(73, 18)
        Me.LblPointJ3.TabIndex = 5
        Me.LblPointJ3.Text = "<valeur>"
        '
        'PnlPointJ3
        '
        Me.PnlPointJ3.Controls.Add(Me.LblNJ3)
        Me.PnlPointJ3.Controls.Add(Me.LblPointJ3)
        Me.PnlPointJ3.Location = New System.Drawing.Point(6, 111)
        Me.PnlPointJ3.Name = "PnlPointJ3"
        Me.PnlPointJ3.Size = New System.Drawing.Size(200, 31)
        Me.PnlPointJ3.TabIndex = 28
        Me.PnlPointJ3.Tag = "3"
        '
        'LblNJ4
        '
        Me.LblNJ4.AutoSize = True
        Me.LblNJ4.Font = New System.Drawing.Font("Corbel", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNJ4.Location = New System.Drawing.Point(3, 12)
        Me.LblNJ4.Name = "LblNJ4"
        Me.LblNJ4.Size = New System.Drawing.Size(99, 18)
        Me.LblNJ4.TabIndex = 1
        Me.LblNJ4.Tag = "4"
        Me.LblNJ4.Text = "Nom joueur 4 :"
        '
        'LblPointJ4
        '
        Me.LblPointJ4.AutoSize = True
        Me.LblPointJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPointJ4.Location = New System.Drawing.Point(121, 12)
        Me.LblPointJ4.Name = "LblPointJ4"
        Me.LblPointJ4.Size = New System.Drawing.Size(73, 18)
        Me.LblPointJ4.TabIndex = 5
        Me.LblPointJ4.Text = "<valeur>"
        '
        'PnlPointJ4
        '
        Me.PnlPointJ4.Controls.Add(Me.LblNJ4)
        Me.PnlPointJ4.Controls.Add(Me.LblPointJ4)
        Me.PnlPointJ4.Location = New System.Drawing.Point(6, 148)
        Me.PnlPointJ4.Name = "PnlPointJ4"
        Me.PnlPointJ4.Size = New System.Drawing.Size(200, 31)
        Me.PnlPointJ4.TabIndex = 28
        Me.PnlPointJ4.Tag = "4"
        '
        'TlpPlateau
        '
        Me.TlpPlateau.AllowDrop = True
        Me.TlpPlateau.AutoScroll = True
        Me.TlpPlateau.AutoSize = True
        Me.TlpPlateau.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TlpPlateau.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.TlpPlateau.ColumnCount = 3
        Me.TlpPlateau.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TlpPlateau.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TlpPlateau.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TlpPlateau.Location = New System.Drawing.Point(567, 233)
        Me.TlpPlateau.Margin = New System.Windows.Forms.Padding(4)
        Me.TlpPlateau.MaximumSize = New System.Drawing.Size(800, 400)
        Me.TlpPlateau.MinimumSize = New System.Drawing.Size(75, 81)
        Me.TlpPlateau.Name = "TlpPlateau"
        Me.TlpPlateau.RowCount = 3
        Me.TlpPlateau.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TlpPlateau.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TlpPlateau.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TlpPlateau.Size = New System.Drawing.Size(75, 81)
        Me.TlpPlateau.TabIndex = 28
        '
        'LblTourJoueur
        '
        Me.LblTourJoueur.AutoSize = True
        Me.LblTourJoueur.Font = New System.Drawing.Font("Papyrus", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTourJoueur.ForeColor = System.Drawing.Color.Black
        Me.LblTourJoueur.Location = New System.Drawing.Point(110, 8)
        Me.LblTourJoueur.Name = "LblTourJoueur"
        Me.LblTourJoueur.Size = New System.Drawing.Size(124, 42)
        Me.LblTourJoueur.TabIndex = 36
        Me.LblTourJoueur.Text = "<valeur>"
        '
        'PnlPointJ1
        '
        Me.PnlPointJ1.Controls.Add(Me.LblNJ1)
        Me.PnlPointJ1.Controls.Add(Me.LblPointJ1)
        Me.PnlPointJ1.Location = New System.Drawing.Point(6, 32)
        Me.PnlPointJ1.Name = "PnlPointJ1"
        Me.PnlPointJ1.Size = New System.Drawing.Size(200, 31)
        Me.PnlPointJ1.TabIndex = 27
        Me.PnlPointJ1.Tag = "1"
        '
        'LblNJ1
        '
        Me.LblNJ1.AutoSize = True
        Me.LblNJ1.Font = New System.Drawing.Font("Corbel", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNJ1.Location = New System.Drawing.Point(3, 12)
        Me.LblNJ1.Name = "LblNJ1"
        Me.LblNJ1.Size = New System.Drawing.Size(98, 18)
        Me.LblNJ1.TabIndex = 1
        Me.LblNJ1.Text = "Nom joueur 1 :"
        '
        'LblPointJ1
        '
        Me.LblPointJ1.AutoSize = True
        Me.LblPointJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPointJ1.Location = New System.Drawing.Point(121, 12)
        Me.LblPointJ1.Name = "LblPointJ1"
        Me.LblPointJ1.Size = New System.Drawing.Size(73, 18)
        Me.LblPointJ1.TabIndex = 5
        Me.LblPointJ1.Text = "<valeur>"
        '
        'LblTour
        '
        Me.LblTour.AutoSize = True
        Me.LblTour.Font = New System.Drawing.Font("Perpetua", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTour.Location = New System.Drawing.Point(26, 16)
        Me.LblTour.Name = "LblTour"
        Me.LblTour.Size = New System.Drawing.Size(94, 28)
        Me.LblTour.TabIndex = 35
        Me.LblTour.Text = "Tour de :"
        '
        'BtnValider
        '
        Me.BtnValider.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnValider.BackColor = System.Drawing.Color.GreenYellow
        Me.BtnValider.Font = New System.Drawing.Font("Waltograph UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnValider.Location = New System.Drawing.Point(1039, 526)
        Me.BtnValider.Name = "BtnValider"
        Me.BtnValider.Size = New System.Drawing.Size(199, 80)
        Me.BtnValider.TabIndex = 33
        Me.BtnValider.Text = "Fin de tour ✔"
        Me.BtnValider.UseVisualStyleBackColor = False
        '
        'gbxPointJoueurs
        '
        Me.gbxPointJoueurs.Controls.Add(Me.PnlPointJ4)
        Me.gbxPointJoueurs.Controls.Add(Me.PnlPointJ3)
        Me.gbxPointJoueurs.Controls.Add(Me.PnlPointJ2)
        Me.gbxPointJoueurs.Controls.Add(Me.PnlPointJ1)
        Me.gbxPointJoueurs.Font = New System.Drawing.Font("Perpetua", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPointJoueurs.Location = New System.Drawing.Point(11, 24)
        Me.gbxPointJoueurs.Name = "gbxPointJoueurs"
        Me.gbxPointJoueurs.Size = New System.Drawing.Size(225, 195)
        Me.gbxPointJoueurs.TabIndex = 38
        Me.gbxPointJoueurs.TabStop = False
        Me.gbxPointJoueurs.Text = "Point joueurs :"
        '
        'LblDescEchangeTuile
        '
        Me.LblDescEchangeTuile.AutoSize = True
        Me.LblDescEchangeTuile.Font = New System.Drawing.Font("Papyrus", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescEchangeTuile.Location = New System.Drawing.Point(14, 19)
        Me.LblDescEchangeTuile.Name = "LblDescEchangeTuile"
        Me.LblDescEchangeTuile.Size = New System.Drawing.Size(133, 24)
        Me.LblDescEchangeTuile.TabIndex = 31
        Me.LblDescEchangeTuile.Text = "Echanges de tuiles"
        '
        'LblNbTuileRestante
        '
        Me.LblNbTuileRestante.AutoSize = True
        Me.LblNbTuileRestante.BackColor = System.Drawing.Color.Transparent
        Me.LblNbTuileRestante.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNbTuileRestante.ForeColor = System.Drawing.Color.Blue
        Me.LblNbTuileRestante.Location = New System.Drawing.Point(67, 6)
        Me.LblNbTuileRestante.Name = "LblNbTuileRestante"
        Me.LblNbTuileRestante.Size = New System.Drawing.Size(58, 24)
        Me.LblNbTuileRestante.TabIndex = 30
        Me.LblNbTuileRestante.Text = "<nb>"
        Me.LblNbTuileRestante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblDescNbTuile
        '
        Me.LblDescNbTuile.AutoSize = True
        Me.LblDescNbTuile.Font = New System.Drawing.Font("Papyrus", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescNbTuile.Location = New System.Drawing.Point(1, 123)
        Me.LblDescNbTuile.Name = "LblDescNbTuile"
        Me.LblDescNbTuile.Size = New System.Drawing.Size(178, 24)
        Me.LblDescNbTuile.TabIndex = 29
        Me.LblDescNbTuile.Text = "Nombre de tuiles restante"
        '
        'BtnAnuller
        '
        Me.BtnAnuller.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnAnuller.BackColor = System.Drawing.Color.IndianRed
        Me.BtnAnuller.Font = New System.Drawing.Font("Waltograph UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAnuller.Location = New System.Drawing.Point(1039, 612)
        Me.BtnAnuller.Name = "BtnAnuller"
        Me.BtnAnuller.Size = New System.Drawing.Size(199, 57)
        Me.BtnAnuller.TabIndex = 34
        Me.BtnAnuller.Text = "Annuler ✘"
        Me.BtnAnuller.UseVisualStyleBackColor = False
        '
        'cmdMute
        '
        Me.cmdMute.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdMute.BackColor = System.Drawing.Color.Azure
        Me.cmdMute.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdMute.Location = New System.Drawing.Point(1073, 12)
        Me.cmdMute.Name = "cmdMute"
        Me.cmdMute.Size = New System.Drawing.Size(84, 65)
        Me.cmdMute.TabIndex = 41
        Me.cmdMute.Text = "🔊"
        Me.cmdMute.UseVisualStyleBackColor = False
        '
        'pnlMainJoueur
        '
        Me.pnlMainJoueur.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlMainJoueur.AutoSize = True
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile1)
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile2)
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile6)
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile3)
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile5)
        Me.pnlMainJoueur.Controls.Add(Me.PtbTuile4)
        Me.pnlMainJoueur.Location = New System.Drawing.Point(412, 569)
        Me.pnlMainJoueur.Name = "pnlMainJoueur"
        Me.pnlMainJoueur.Padding = New System.Windows.Forms.Padding(3)
        Me.pnlMainJoueur.Size = New System.Drawing.Size(353, 76)
        Me.pnlMainJoueur.TabIndex = 43
        Me.pnlMainJoueur.Text = "Main Joueur"
        '
        'lblReroll
        '
        Me.lblReroll.AutoSize = True
        Me.lblReroll.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReroll.Location = New System.Drawing.Point(116, 75)
        Me.lblReroll.Name = "lblReroll"
        Me.lblReroll.Size = New System.Drawing.Size(0, 25)
        Me.lblReroll.TabIndex = 44
        '
        'pnlReroll
        '
        Me.pnlReroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlReroll.AutoSize = True
        Me.pnlReroll.Controls.Add(Me.PtbEchangeTuile)
        Me.pnlReroll.Controls.Add(Me.LblDescEchangeTuile)
        Me.pnlReroll.Controls.Add(Me.lblReroll)
        Me.pnlReroll.Location = New System.Drawing.Point(64, 509)
        Me.pnlReroll.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlReroll.Name = "pnlReroll"
        Me.pnlReroll.Size = New System.Drawing.Size(153, 136)
        Me.pnlReroll.TabIndex = 45
        '
        'pnlPioche
        '
        Me.pnlPioche.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pnlPioche.Controls.Add(Me.LblNbTuileRestante)
        Me.pnlPioche.Controls.Add(Me.LblDescNbTuile)
        Me.pnlPioche.Controls.Add(Me.PtbPioche)
        Me.pnlPioche.Location = New System.Drawing.Point(38, 284)
        Me.pnlPioche.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlPioche.Name = "pnlPioche"
        Me.pnlPioche.Size = New System.Drawing.Size(175, 152)
        Me.pnlPioche.TabIndex = 46
        '
        'pnlTour
        '
        Me.pnlTour.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.pnlTour.AutoSize = True
        Me.pnlTour.Controls.Add(Me.LblTourJoueur)
        Me.pnlTour.Controls.Add(Me.LblTour)
        Me.pnlTour.Location = New System.Drawing.Point(450, 19)
        Me.pnlTour.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlTour.Name = "pnlTour"
        Me.pnlTour.Size = New System.Drawing.Size(250, 58)
        Me.pnlTour.TabIndex = 47
        '
        'PtbPioche
        '
        Me.PtbPioche.Image = Global.QwirkleIHM.My.Resources.Resources.box
        Me.PtbPioche.Location = New System.Drawing.Point(33, 20)
        Me.PtbPioche.Name = "PtbPioche"
        Me.PtbPioche.Size = New System.Drawing.Size(107, 100)
        Me.PtbPioche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbPioche.TabIndex = 28
        Me.PtbPioche.TabStop = False
        '
        'PtbEchangeTuile
        '
        Me.PtbEchangeTuile.BackColor = System.Drawing.Color.Red
        Me.PtbEchangeTuile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PtbEchangeTuile.Location = New System.Drawing.Point(34, 46)
        Me.PtbEchangeTuile.Name = "PtbEchangeTuile"
        Me.PtbEchangeTuile.Size = New System.Drawing.Size(76, 77)
        Me.PtbEchangeTuile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbEchangeTuile.TabIndex = 32
        Me.PtbEchangeTuile.TabStop = False
        '
        'PtbTuile1
        '
        Me.PtbTuile1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile1.Location = New System.Drawing.Point(14, 13)
        Me.PtbTuile1.Name = "PtbTuile1"
        Me.PtbTuile1.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile1.TabIndex = 15
        Me.PtbTuile1.TabStop = False
        Me.PtbTuile1.Tag = "0"
        '
        'PtbTuile2
        '
        Me.PtbTuile2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile2.Location = New System.Drawing.Point(69, 13)
        Me.PtbTuile2.Name = "PtbTuile2"
        Me.PtbTuile2.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile2.TabIndex = 16
        Me.PtbTuile2.TabStop = False
        Me.PtbTuile2.Tag = "1"
        '
        'PtbTuile6
        '
        Me.PtbTuile6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile6.Location = New System.Drawing.Point(291, 13)
        Me.PtbTuile6.Name = "PtbTuile6"
        Me.PtbTuile6.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile6.TabIndex = 20
        Me.PtbTuile6.TabStop = False
        Me.PtbTuile6.Tag = "5"
        '
        'PtbTuile3
        '
        Me.PtbTuile3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile3.Location = New System.Drawing.Point(124, 13)
        Me.PtbTuile3.Name = "PtbTuile3"
        Me.PtbTuile3.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile3.TabIndex = 17
        Me.PtbTuile3.TabStop = False
        Me.PtbTuile3.Tag = "2"
        '
        'PtbTuile5
        '
        Me.PtbTuile5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile5.Location = New System.Drawing.Point(236, 13)
        Me.PtbTuile5.Name = "PtbTuile5"
        Me.PtbTuile5.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile5.TabIndex = 19
        Me.PtbTuile5.TabStop = False
        Me.PtbTuile5.Tag = "4"
        '
        'PtbTuile4
        '
        Me.PtbTuile4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PtbTuile4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PtbTuile4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PtbTuile4.Location = New System.Drawing.Point(180, 13)
        Me.PtbTuile4.Name = "PtbTuile4"
        Me.PtbTuile4.Size = New System.Drawing.Size(50, 50)
        Me.PtbTuile4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PtbTuile4.TabIndex = 18
        Me.PtbTuile4.TabStop = False
        Me.PtbTuile4.Tag = "3"
        '
        'FrmJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1264, 681)
        Me.Controls.Add(Me.pnlTour)
        Me.Controls.Add(Me.pnlPioche)
        Me.Controls.Add(Me.pnlReroll)
        Me.Controls.Add(Me.pnlMainJoueur)
        Me.Controls.Add(Me.cmdMute)
        Me.Controls.Add(Me.TlpPlateau)
        Me.Controls.Add(Me.BtnValider)
        Me.Controls.Add(Me.gbxPointJoueurs)
        Me.Controls.Add(Me.BtnAnuller)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(964, 592)
        Me.Name = "FrmJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Jeu en cours"
        Me.PnlPointJ2.ResumeLayout(False)
        Me.PnlPointJ2.PerformLayout()
        Me.PnlPointJ3.ResumeLayout(False)
        Me.PnlPointJ3.PerformLayout()
        Me.PnlPointJ4.ResumeLayout(False)
        Me.PnlPointJ4.PerformLayout()
        Me.PnlPointJ1.ResumeLayout(False)
        Me.PnlPointJ1.PerformLayout()
        Me.gbxPointJoueurs.ResumeLayout(False)
        Me.pnlMainJoueur.ResumeLayout(False)
        Me.pnlReroll.ResumeLayout(False)
        Me.pnlReroll.PerformLayout()
        Me.pnlPioche.ResumeLayout(False)
        Me.pnlPioche.PerformLayout()
        Me.pnlTour.ResumeLayout(False)
        Me.pnlTour.PerformLayout()
        CType(Me.PtbPioche, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbEchangeTuile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbTuile4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PtbTuile1 As PictureBox
    Friend WithEvents PtbTuile2 As PictureBox
    Friend WithEvents PtbTuile3 As PictureBox
    Friend WithEvents PtbTuile4 As PictureBox
    Friend WithEvents PtbTuile5 As PictureBox
    Friend WithEvents PtbTuile6 As PictureBox
    Friend WithEvents LblNJ2 As Label
    Friend WithEvents LblPointJ2 As Label
    Friend WithEvents PnlPointJ2 As Panel
    Friend WithEvents LblNJ3 As Label
    Friend WithEvents LblPointJ3 As Label
    Friend WithEvents PnlPointJ3 As Panel
    Friend WithEvents LblNJ4 As Label
    Friend WithEvents LblPointJ4 As Label
    Friend WithEvents PnlPointJ4 As Panel
    Friend WithEvents TlpPlateau As TableLayoutPanel
    Friend WithEvents LblTourJoueur As Label
    Friend WithEvents PnlPointJ1 As Panel
    Friend WithEvents LblNJ1 As Label
    Friend WithEvents LblPointJ1 As Label
    Friend WithEvents LblTour As Label
    Friend WithEvents BtnValider As Button
    Friend WithEvents gbxPointJoueurs As GroupBox
    Friend WithEvents PtbEchangeTuile As PictureBox
    Friend WithEvents LblDescEchangeTuile As Label
    Friend WithEvents LblNbTuileRestante As Label
    Friend WithEvents LblDescNbTuile As Label
    Friend WithEvents PtbPioche As PictureBox
    Friend WithEvents BtnAnuller As Button
    Friend WithEvents cmdMute As Button
    Friend WithEvents pnlMainJoueur As Panel
    Friend WithEvents lblReroll As Label
    Friend WithEvents pnlReroll As Panel
    Friend WithEvents pnlPioche As Panel
    Friend WithEvents pnlTour As Panel
End Class
