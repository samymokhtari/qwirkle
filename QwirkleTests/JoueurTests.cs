﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;


namespace QwirkleTests
{
    [TestClass]
    public class JoueurTests
    {
        [TestMethod]
        public void TestGetNom()
        {
            Joueur joueur = new Joueur("Samy", 25, 04, 1999);
            Assert.AreEqual("Samy", joueur.Nom);
        }

        [TestMethod]
        public void TestGetPoints()
        {
            Joueur joueur = new Joueur("Samy", 25, 04, 1999);
            Assert.AreEqual(0, joueur.Point);
        }

        [TestMethod]
        public void TestGetJourNais()
        {
            Joueur joueur = new Joueur("Samy", 25, 04, 1999);
            Assert.AreEqual(25, joueur.JourNais);
        }

        [TestMethod]
        public void TestGetMoisNais()
        {
            Joueur joueur = new Joueur("Samy", 25, 04, 1999);
            Assert.AreEqual(04, joueur.MoisNais);
        }

        [TestMethod]
        public void TestGetAnneeNais()
        {
            Joueur joueur = new Joueur("Samy", 25, 04, 1999);
            Assert.AreEqual(1999, joueur.AnneeNais);
        }

        [TestMethod]
        public void TestRemplirMain()
        {
            Pioche p_pioche = new Pioche();
            Joueur joueur = new Joueur("Léo", 07, 09, 1998);
            joueur.RemplirMain(p_pioche);
            Assert.AreEqual(6, joueur.MainJoueur.Count);
        }

        [TestMethod]
        public void TestComptagePoint()
        {
            Joueur Leo = new Joueur("Léo", 07, 09, 1998);
            List<Tuile> listTuile = new List<Tuile>();
            Tuile tuile1 = new Tuile("rond", "rouge");
            Tuile tuile2 = new Tuile("rond", "bleu");
            Tuile tuile3 = new Tuile("rond", "Jaune");
            Tuile tuile4 = new Tuile("rond", "vert");
            listTuile.Add(tuile1);
            listTuile.Add(tuile2);
            listTuile.Add(tuile3);
            listTuile.Add(tuile4);
            Leo.ComptagePoint(listTuile);

            Assert.AreEqual(4, Leo.Point);
        }

        [TestMethod]
        public void TestJoue()
        {
            Pioche pioche = new Pioche();
            Joueur Leo = new Joueur("Léo", 07, 09, 1998);

            Leo.RemplirMain(pioche);
            Leo.Joue(Leo.MainJoueur[0]);

            Assert.AreEqual(5, Leo.MainJoueur.Count);
        }

        [TestMethod]
        public void TestFinTourPoseTuile()
        {
            Pioche pioche = new Pioche();
            Joueur Leo = new Joueur("Léo", 07, 09, 1998);
            Tuile tuile1 = new Tuile("Rond", "Rouge");
            Tuile tuile2 = new Tuile("rond", "bleu");
            Tuile tuile3 = new Tuile("rond", "Jaune");
            Tuile tuile4 = new Tuile("rond", "vert");
            Tuile tuile5 = new Tuile("rond", "violet");
            Tuile tuile6 = new Tuile("rond", "orange");


            //Si le plateau est vide et que le joueur pose une tuile
            List<Tuile> mot1 = new List<Tuile>();
            List<Tuile> mot2 = new List<Tuile>();
            List<List<Tuile>> mots1 = new List<List<Tuile>>
            {
                mot1,
                mot2
            };
            mot1.Add(tuile1);
            mot2.Add(tuile1);
            Assert.AreEqual(false, Leo.FinDeTour(pioche, mots1));
            Assert.AreEqual(1, Leo.Point);

            //Si le plateau est vide et que le joueur pose deux tuiles
            mot2.Add(tuile2);
            Assert.AreEqual(false, Leo.FinDeTour(pioche, mots1));
            Assert.AreEqual(3, Leo.Point);//points precedants(1) et ceux ajoutés(2)

            //Si le joueur pose deux tuiles et 2 tuiles alignés deja presentes
            List<Tuile> mot3 = new List<Tuile>();
            mot3.Add(tuile1);
            mot3.Add(tuile2);
            mot3.Add(tuile3);
            mot3.Add(tuile4);
            List<Tuile> mot4 = new List<Tuile>();
            mot4.Add(tuile3);
            List<Tuile> mot5 = new List<Tuile>();
            mot5.Add(tuile4);

            List<List<Tuile>> mots2 = new List<List<Tuile>>()
            {
                mot3,
                mot4,
                mot5
            };
            Assert.AreEqual(false, Leo.FinDeTour(pioche, mots2));
            Assert.AreEqual(7, Leo.Point);

            //Pose de deux tuiles formant trois qwirkle, pioche vide et main vide apres avoir jouer
            Joueur Thibaut = new Joueur("Thibaut", 22, 05, 2000);
            Thibaut.MainJoueur.Add(tuile5);
            Thibaut.MainJoueur.Add(tuile6);
            for (int nb_boucle = 0; nb_boucle < 102; nb_boucle++)
            {
                pioche.DistributionTuile();
            }
            List<Tuile> mot6 = new List<Tuile>();
            mot6.Add(tuile1);
            mot6.Add(tuile2);
            mot6.Add(tuile3);
            mot6.Add(tuile4);
            List<Tuile> mot7 = new List<Tuile>();
            mot7.Add(tuile1);
            mot7.Add(tuile2);
            mot7.Add(tuile3);
            mot7.Add(tuile4);
            mot7.Add(tuile5);
            List<Tuile> mot8 = new List<Tuile>();
            mot8.Add(tuile1);
            mot8.Add(tuile2);
            mot8.Add(tuile3);
            mot8.Add(tuile4);
            mot8.Add(tuile6);
            //Pose des tuiles
            mot6.Add(tuile5);
            mot8.Add(tuile5);
            Thibaut.Joue(tuile5);
            mot6.Add(tuile6);
            mot7.Add(tuile6);
            Thibaut.Joue(tuile6);

            List<List<Tuile>> mots3 = new List<List<Tuile>>()
            {
                mot6,
                mot7,
                mot8
            };
            Assert.AreEqual(true, Thibaut.FinDeTour(pioche, mots3));
            Assert.AreEqual(42, Thibaut.Point);
        }

        [TestMethod]
        public void TestAfficherMain()
        {
            Joueur joueur = new Joueur("Léo", 07, 09, 1998);
            Tuile tuile1 = new Tuile("rond", "jaune");
            joueur.MainJoueur.Add(tuile1);
            Assert.AreEqual(tuile1, joueur.RecupererTuile(0));
        }

        [TestMethod]
        public void TestReroll()
        {
            Pioche pioche = new Pioche();
            List<Tuile> ListeTuile = new List<Tuile>();
            Joueur joueur = new Joueur("Léo", 07, 09, 1998);
            joueur.RemplirMain(pioche);

            joueur.Reroll(pioche, joueur.MainJoueur[0]);
            Assert.AreEqual(5, joueur.MainJoueur.Count);
        }

        [TestMethod]
        public void TestFinTourReroll()
        {
            Pioche pioche = new Pioche();
            List<Tuile> ListeTuile = new List<Tuile>();
            Joueur joueur = new Joueur("Léo", 07, 09, 1998);
            joueur.RemplirMain(pioche);
            List<Tuile> TuileARepiocher = new List<Tuile>
            {
                joueur.MainJoueur[0],
                joueur.MainJoueur[1],
                joueur.MainJoueur[2]
            };
            joueur.FinDeTour(pioche, TuileARepiocher);
            Assert.AreEqual(6, joueur.MainJoueur.Count);
        }
    }
}
