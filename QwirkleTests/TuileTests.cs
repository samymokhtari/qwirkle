﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace QwirkleTests
{
    [TestClass]
    public class TuileTests
    {
        [TestMethod]
        public void TestCompatible_1arg()
        {
            Tuile tuile1 = new Tuile("Carre", "Rouge");
            Tuile tuile2 = new Tuile("Carre", "Violet");
            Tuile tuile3 = new Tuile("Carre", "Jaune");
            Tuile tuile4 = new Tuile("Etoile", "Rouge");
            Tuile tuile5 = new Tuile("Croix", "Rouge");
            Tuile tuile6 = new Tuile("Rond", "Vert");
            Tuile tuile7 = new Tuile("Losange", "Orange");
            Tuile tuile8 = new Tuile("Carre", "Rouge");

            List<Tuile> tuiles = new List<Tuile>
            {
                tuile2,
                tuile3
            };
            Assert.AreEqual(true, tuile1.Compatible(tuiles));
            tuiles.Add(tuile8);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));
            tuiles.RemoveAt(tuiles.Count-1);
            tuiles.Add(tuile6);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));

            tuiles.Clear();
            tuiles.Add(tuile4);
            tuiles.Add(tuile5);
            Assert.AreEqual(true, tuile1.Compatible(tuiles));
            tuiles.Add(tuile8);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));
            tuiles.RemoveAt(tuiles.Count - 1);
            tuiles.Add(tuile6);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));

            tuiles.Clear();
            tuiles.Add(tuile6);
            tuiles.Add(tuile7);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));
            tuiles.Add(tuile8);
            Assert.AreEqual(false, tuile1.Compatible(tuiles));
        }
    }
}
