﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace QwirkleTests
{
    [TestClass]
    public class PiocheTests
    {
        [TestMethod]
        public void AjouterTuileTest()
        {
            Tuile tuile = new Tuile("Rond", "Bleu");
            Pioche pioche = new Pioche();
            pioche.AjouterTuile(tuile);
            Assert.AreEqual(109, pioche.CountTuiles);
        }

        [TestMethod]
        public void RetirerTuileTest()
        {
            Pioche pioche = new Pioche();
            pioche.DistributionTuile();
            Assert.AreEqual(107, pioche.CountTuiles);
        }

    }
}
