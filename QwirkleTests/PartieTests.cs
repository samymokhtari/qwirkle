﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace QwirkleTests
{
    [TestClass]
    public class PartieTests
    {
        [TestMethod]
        public void TestOrdonnancementJoueurs()
        {
            List<int> listMot = new List<int>();
            Partie partie = new Partie(3);
            Joueur J1 = new Joueur("Nom Joueur 1", 01, 01, 1800);
            Joueur J2 = new Joueur("Nom Joueur 2", 01, 01, 1800);
            Joueur J3 = new Joueur("Nom Joueur 3", 01, 01, 1800);

            partie.AjouterJoueur(0,J1);
            partie.AjouterJoueur(1,J2);
            partie.AjouterJoueur(2,J3);
            listMot.Add(2);
            listMot.Add(1);
            listMot.Add(4);

            partie.InitiliasationOrdreJoueur(listMot);
            //Normalement l'ordre = j2,j4,j3,j1
            Assert.AreEqual(J3, partie.GetJoueurOf(0));
            Assert.AreEqual(J1, partie.GetJoueurOf(1));
            Assert.AreEqual(J2, partie.GetJoueurOf(2));
        }

        [TestMethod]
        public void TestComparaisonDateNaissance()
        {
            Joueur J1 = new Joueur("sam", 25, 04, 1999);
            Joueur J2 = new Joueur("alex", 18, 06, 1999);
            Assert.AreEqual(true,Partie.ComparaisonDateNaissance(J1, J2));
        }


    }
}
